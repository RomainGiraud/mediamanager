package mock;

import javax.swing.JFrame;
import javax.swing.JPanel;

import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.controller.SearchController;

/**A window used to test the search view.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class SearchView {
	
	public static void main(String[] args) {
		JFrame window = new JFrame("Test search window");
		
		SearchInput input = new SearchInput();
		
		SearchController<Item> controller = new SearchController<Item>(input);
		
		JPanel panel = controller.getView();
		window.setContentPane(panel);
		window.pack();
		window.setSize(1200, 400);
		window.setVisible(true);
	}
	
}