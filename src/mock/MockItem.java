package mock;

import java.util.List;
import java.util.ArrayList;

/**Mock class for the Item superclass.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class MockItem extends Item {
	public MockItem(String salt) {
		super(salt);
	}
	
	public List<String> metadataKeys() {
		List<String> result = new ArrayList<String>(3);
		result.add("Titre");
		result.add("Auteur");
		return result;
	}
}