package mock;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**Mock class for the Item superclass.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public abstract class Item {
	/**Keys are the ones from metadataKeys, values are the actual values for this item.
	 *@see	metadataKeys
	 */
	protected Map<String, String> meta;
	
	protected Item() {
		this("default");
	}
	
	protected Item(String salt) {
		for (String s : metadataKeys())
			meta.put(s, "Dummy value for " + s + " ; " + salt);
	}
	
	public abstract List<String> metadataKeys();
	
	public String getMeta(String key) {
		if (! metadataKeys().contains(key))
			throw new IllegalArgumentException("Invalid metadata key \"" + key + "\" for a " + this.getClass().getSimpleName() + " !");
		return meta.get(key);
	}
	
	public Item setMeta(String key, String value) {
		if (! metadataKeys().contains(key))
			throw new IllegalArgumentException("Invalid metadata key \"" + key + "\" for a " + this.getClass().getSimpleName() + " !");
		meta.put(key, value);
		return this;
	}
}