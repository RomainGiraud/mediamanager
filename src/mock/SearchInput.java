package mock;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.controller.SearchController;
import search.sample.TextualCondition;
import search.model.PrintTypes;
import search.client.SearchInputProvider;

/**A window used to test the search view.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class SearchInput implements SearchInputProvider<Item> {
	
	public Map<String, ElementaryCondition<Item>> getConditions() {
		Map<String, ElementaryCondition<Item>> result = new HashMap<String, ElementaryCondition<Item>>();
		result.put("L'auteur", new TextualCondition<Item>() {
					public Boolean eval(Item i) {
						return eval(i.getMeta("Auteur"));
					}
				   
				   protected String prefix(PrintTypes type) {
					return "L'auteur";
				   }
				}
		);
		
		result.put("Le titre", new TextualCondition<Item>() {
				public Boolean eval(Item i) {
					return eval(i.getMeta("Titre"));
				}
				   
			   public String prefix(PrintTypes type) {
				   return "Le titre";
			   }
			}
		);
		return result;
	}
	
	public List<Item> getData() {
		List<Item> result = new LinkedList<Item>();
		result.add(new MockItem("1"));
		result.add(new MockItem("2"));
		result.add(new MockItem("3"));
		return result;
	}
}