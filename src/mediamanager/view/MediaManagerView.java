package mediamanager.view;
import fr.unice.polytech.mediamanager.model.*;
import mediamanager.model.Manager;
import mediamanager.event.ManagerListener;
import mediamanager.controller.Controller;
import mediamanager.dialog.DialogNewFilm;

import search.client.SearchListener;

import java.util.List;
import java.util.LinkedList;

import javax.swing.JList;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JOptionPane;

import java.awt.Insets;
import java.awt.Color;
import java.awt.Component;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;

public class MediaManagerView extends JFrame implements ManagerListener, SearchListener<Film> {
    private Manager    model;
    private Controller controller;
	
	private List<Film> items = new LinkedList<Film>();

    private JPanel superMainPanel   = new JPanel();
    private JPanel addFilmPanel     = new JPanel();
    private JPanel addActorPanel    = new JPanel();
    private JPanel addDirectorPanel = new JPanel();

    private JScrollPane scrollAddPanel = new JScrollPane();

    private JButton addNewFilm    = new JButton("ajouter");
    private JButton cancelNewFilm = new JButton("annuler");
    private FilmAddView filmAddView;

    private JPanel mainPanel  = new JPanel();
    private JPanel topPanel   = new JPanel();
	
	private JPanel searchPanel;

    private JPanel        microFilmsPanel       = new JPanel();
    private JScrollPane   scrollMicroFilmsPanel = new JScrollPane(microFilmsPanel);
    private GridBagLayout layoutMicroFilmsPanel = new GridBagLayout();
    private JLabel        tipEndLayout          = new JLabel();

    private JPanel        mediumFilmsPanel       = new JPanel();
    private JScrollPane   scrollMediumFilmsPanel = new JScrollPane(mediumFilmsPanel);

    private JPanel        superMacroFilmsPanel  = new JPanel();
    private JPanel        macroFilmsPanel       = new JPanel();
    private JScrollPane   scrollMacroFilmsPanel = new JScrollPane(macroFilmsPanel);

    private JComboBox viewsCombo = new JComboBox(new String[] {"micro", "medium", "macro"});

    private FilmToView filmToView;

    private JPanel centerPanel = new JPanel();

    class MyItemListener implements ItemListener {
        private String command = "";
        public MyItemListener(String command) {
            this.command = command;
        }
        public void itemStateChanged(ItemEvent e) {
            boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
            for (FilmMicroView f : filmToView.getAllMicroViews())
                f.setVisible(command, selected);
            for (FilmMediumView f : filmToView.getAllMediumViews())
                f.setVisible(command, selected);
            for (FilmMacroView f : filmToView.getAllMacroViews())
                f.setVisible(command, selected);
        }
    }

    public MediaManagerView(Controller c, Manager m, JPanel searchView) {
        this.model      = m;
        this.controller = c;
		this.searchPanel = searchView;

        this.model.addManagerListener(this);
		

        filmToView = new FilmToView(this.controller);

        // panel du haut
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
        topPanel.add(new JLabel("Afficher"));

        JCheckBox displayTitle = new JCheckBox("titre");
        displayTitle.addItemListener(new MyItemListener("title"));
        displayTitle.setSelected(true);
        topPanel.add(displayTitle);

        JCheckBox displaySynopsis = new JCheckBox("synopsis");
        displaySynopsis.addItemListener(new MyItemListener("synopsis"));
        displaySynopsis.setSelected(true);
        topPanel.add(displaySynopsis);

        addFilmPanel.setLayout(new BoxLayout(addFilmPanel, BoxLayout.Y_AXIS));

        addNewFilm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (filmAddView.filmValid()) {
                    Film newFilm = new Film(filmAddView.getId(), filmAddView.getTitle(), filmAddView.getDirector(), filmAddView.getActors(), filmAddView.getGenres(), filmAddView.getRuntime(), filmAddView.getPoster(), filmAddView.getSynopsis());

                    boolean ret;
                    if (filmAddView.getId() == "") {
                        ret = controller.addFilm(newFilm);
                    } else {
                        ret = controller.updateFilm(newFilm);
                    }

                    if (!ret) {
                        JOptionPane.showMessageDialog(null, "Impossible d'insérer le film", "Problème", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs.", "Problème", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                closeAddFilm();
            }
        });

        cancelNewFilm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeAddFilm();
            }
        });

        topPanel.add(Box.createHorizontalGlue());
        // bouton ajouter
        JButton addButton = new JButton("+");
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                openAddFilm();
            }
        });
        topPanel.add(addButton);
        // bouton supprimer
        JButton delButton = new JButton("-");
        delButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        topPanel.add(delButton);
        // bouton basculer de vue
        viewsCombo.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    CardLayout cl = (CardLayout)(centerPanel.getLayout());
                    cl.show(centerPanel, e.getItem().toString());
                    repaintListItems();
                }
            }
        });
        topPanel.add(viewsCombo);

        microFilmsPanel.setLayout(layoutMicroFilmsPanel);
        setListItems();

        mediumFilmsPanel.setLayout(new BoxLayout(mediumFilmsPanel, BoxLayout.Y_AXIS));

        JPanel navigationPanelMacroFilms = new JPanel();
        navigationPanelMacroFilms.setLayout(new BoxLayout(navigationPanelMacroFilms, BoxLayout.X_AXIS));
        JButton previousButton = new JButton("<<");
        previousButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout)(macroFilmsPanel.getLayout());
                cl.previous(macroFilmsPanel);
            }
        });
        navigationPanelMacroFilms.add(previousButton);
        navigationPanelMacroFilms.add(Box.createHorizontalGlue());
        JButton nextButton = new JButton(">>");
        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout)(macroFilmsPanel.getLayout());
                cl.next(macroFilmsPanel);
            }
        });
        navigationPanelMacroFilms.add(nextButton);
        macroFilmsPanel.setLayout(new CardLayout());
        superMacroFilmsPanel.setLayout(new BoxLayout(superMacroFilmsPanel, BoxLayout.Y_AXIS));
        superMacroFilmsPanel.add(macroFilmsPanel);
        superMacroFilmsPanel.add(navigationPanelMacroFilms);

        centerPanel.setLayout(new CardLayout());
        centerPanel.add(scrollMicroFilmsPanel,  "micro");
        centerPanel.add(scrollMediumFilmsPanel, "medium");
        centerPanel.add(superMacroFilmsPanel,   "macro");

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(topPanel);
		mainPanel.add(searchPanel);
        mainPanel.add(centerPanel);

        superMainPanel.setLayout(new CardLayout());
        superMainPanel.add(mainPanel, "main");

        getContentPane().add(superMainPanel, BorderLayout.CENTER);

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                if (e.getID() == ComponentEvent.COMPONENT_RESIZED) {
                    repaintListItems();
                }
            }
        });
        
        setSize(600,400);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
	
	public void onSearchResultsRecieved(List<Film> films) {
		setListItems(films);
	}

    private void closeAddFilm() {
        CardLayout cl = (CardLayout)(superMainPanel.getLayout());
        cl.removeLayoutComponent(addFilmPanel);
        addFilmPanel.remove(scrollAddPanel);
        addFilmPanel.remove(addNewFilm);
        addFilmPanel.remove(cancelNewFilm);
    }

    public void openAddFilm(Film f) {
        openAddFilm();
        filmAddView.populate(f);
    }

    public void openAddFilm() {
        filmAddView = new FilmAddView(model.getAllGenres(), model.getAllActors(), model.getAllDirectors());
        scrollAddPanel = new JScrollPane(filmAddView);

        addFilmPanel.add(scrollAddPanel);
        addFilmPanel.add(addNewFilm);
        addFilmPanel.add(cancelNewFilm);

        superMainPanel.add(addFilmPanel, "add");

        CardLayout cl = (CardLayout)(superMainPanel.getLayout());
        cl.show(superMainPanel, "add");
    }

    private void setListItems() {
		setListItems(items);
	}

    private void setListItems(List<Film> films) {
        filmToView.clearAll();
        microFilmsPanel.removeAll();
        mediumFilmsPanel.removeAll();
        macroFilmsPanel.removeAll();
        for (Film f : films) {
            microFilmsPanel.add(filmToView.getMicro(f));
            mediumFilmsPanel.add(filmToView.getMedium(f));
            macroFilmsPanel.add(filmToView.getMacro(f), f.getId());
        }
        microFilmsPanel.add(tipEndLayout);
        mediumFilmsPanel.add(Box.createVerticalGlue());
        mediumFilmsPanel.revalidate();
        macroFilmsPanel.revalidate();
        macroFilmsPanel.repaint();
		items = films;
        repaintListItems();
    }

    private void repaintListItems() {
        Collection<FilmMicroView> microViews = filmToView.getAllMicroViews();

        if (microViews.size() == 0) return;

        int count = 0;

        int nbMin = scrollMicroFilmsPanel.getSize().width / FilmMicroView.WIDTH_MIN;
        int nbMax = scrollMicroFilmsPanel.getSize().width / FilmMicroView.WIDTH_MAX;
        int nb = 1;
        if (nbMin > nbMax)
            nb = nbMin;
        else
            nb = nbMax;
        if (nb <= 0) nb = 1;

        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 0.5;
        c.weighty = 0.0;
        c.anchor  = GridBagConstraints.FIRST_LINE_START;
        c.fill    = GridBagConstraints.BOTH;
        c.insets  = new Insets(2, 2, 2, 2);

        for (FilmMicroView f : microViews) {
            if (count++ % nb == 0)
                ++c.gridy;
            layoutMicroFilmsPanel.setConstraints(f, c);
        }

        // astuce pour une glue en bas du layout
        ++c.gridy;
        c.weightx = 1;
        c.weighty = 1;
        layoutMicroFilmsPanel.setConstraints(tipEndLayout, c);

        microFilmsPanel.revalidate();
    }

    public void filmAdded(Film film) {
		items.add(film);
        setListItems();
    }

    public void filmDeleted(Film film) {
		items.remove(film);
        setListItems();
    }

    public void filmUpdated(Film film) {
        //evaluate(model.getAllFilms());
        items = model.getAllFilms();
        setListItems();
    }
}

class FilmToView {
    private Controller controller;
    private HashMap<String, Film>         hash         = new HashMap<String, Film>();
    private HashMap<Film, FilmMicroView>  filmToMicro  = new HashMap<Film, FilmMicroView>();
    private HashMap<Film, FilmMediumView> filmToMedium = new HashMap<Film, FilmMediumView>();
    private HashMap<Film, FilmMacroView>  filmToMacro  = new HashMap<Film, FilmMacroView>();

    public FilmToView(Controller c) {
        this.controller = c;
    }

    public Collection<FilmMicroView> getAllMicroViews() {
        return filmToMicro.values();
    }

    public Collection<FilmMediumView> getAllMediumViews() {
        return filmToMedium.values();
    }

    public Collection<FilmMacroView> getAllMacroViews() {
        return filmToMacro.values();
    }

    public FilmMicroView getMicro(Film film) {
        Film f = hash.get(film.getId());

        if (f == null) {
            add(film);
            f = hash.get(film.getId());
        }

        return filmToMicro.get(f);
    }

    public FilmMediumView getMedium(Film film) {
        Film f = hash.get(film.getId());

        if (f == null) {
            add(film);
            f = hash.get(film.getId());
        }

        return filmToMedium.get(f);
    }

    public FilmMacroView getMacro(Film film) {
        Film f = hash.get(film.getId());

        if (f == null) {
            add(film);
            f = hash.get(film.getId());
        }

        return filmToMacro.get(f);
    }

    public void add(Film f) {
        String id = f.getId();
        if (hash.containsKey(id)) return;

        hash.put(id, f);
        filmToMicro.put (f, new FilmMicroView(f));
        filmToMedium.put(f, new FilmMediumView(f));
        filmToMacro.put (f, new FilmMacroView(f, controller));
    }

    public void delete(Film f) {
        String id = f.getId();
        if (!hash.containsKey(id)) return;

        f = hash.get(id);
        hash.remove(id);
        filmToMicro.remove(f);
        filmToMedium.remove(f);
        filmToMacro.remove(f);
    }

    public void clearAll() {
        hash.clear();
        filmToMicro.clear();
        filmToMedium.clear();
        filmToMacro.clear();
    }
}
