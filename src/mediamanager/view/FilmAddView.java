package mediamanager.view;

import fr.unice.polytech.mediamanager.model.*;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class FilmAddView extends JPanel {
    private JTextField   titleEditor        = new JTextField();
    private JTextArea    synopsisEditor     = new JTextArea();
    private SpinnerModel runtimeModelEditor = new SpinnerNumberModel();
    private JSpinner     runtimeEditor      = new JSpinner(runtimeModelEditor);
    private JTextField   posterEditor       = new JTextField();
    private JList        genresEditor       = new JList();
    private JList        actorsEditor       = new JList();
    private JList        dirsEditor         = new JList();

    private String id = "";

    private ArrayList<Actor>    actors;
    private ArrayList<Genre>    genres;
    private ArrayList<Director> directors;

    public String getId() { return id; }
    public String getTitle() { return titleEditor.getText(); }
    public String getSynopsis() { return synopsisEditor.getText(); }
    public int getRuntime() { return (Integer) runtimeModelEditor.getValue(); }
    public String getPoster() { return posterEditor.getText(); }
    public Director getDirector() { return directors.get(dirsEditor.getSelectedIndex()); }
    public ArrayList<Actor> getActors() {
        ArrayList<Actor> a = new ArrayList<Actor>();
        for (int i : actorsEditor.getSelectedIndices())
            a.add(actors.get(i));
        return a;
    }
    public ArrayList<Genre> getGenres() {
        ArrayList<Genre> g = new ArrayList<Genre>();
        for (int i : genresEditor.getSelectedIndices())
            g.add(genres.get(i));
        return g;
    }

    public boolean filmValid() {
        return !getTitle().isEmpty() && !getSynopsis().isEmpty() && dirsEditor.getSelectedIndex() != -1 && actorsEditor.getSelectedIndices().length != 0 && genresEditor.getSelectedIndices().length != 0;
    }

    public void populate(Film f) {
        id = f.getId();
        titleEditor.setText(f.getTitle());
        synopsisEditor.setText(f.getSynopsis());
        runtimeEditor.setValue(f.getRuntime());
        posterEditor.setText(f.getPoster());

        for (Director d : directors)
            if (d.getId() == f.getDirector().getId())
                dirsEditor.setSelectedIndex(directors.indexOf(d));

        ArrayList<Integer> selection = new ArrayList<Integer>();
        for (Genre g : f.getGenres())
            selection.add(genres.indexOf(g));
        int[] selectionArray = new int[selection.size()];
        for (int i = 0; i < selectionArray.length; ++i)
            selectionArray[i] = selection.get(i);
        genresEditor.setSelectedIndices(selectionArray);

        selection.clear();
        for (Actor a : f.getActors())
            for (Actor a2 : actors)
                if (a.getId() == a2.getId())
                    selection.add(actors.indexOf(a2));
        selectionArray = new int[selection.size()];
        for (int i = 0; i < selectionArray.length; ++i)
            selectionArray[i] = selection.get(i);
        actorsEditor.setSelectedIndices(selectionArray);
    }

    public FilmAddView(ArrayList<Genre> genres, ArrayList<Actor> actors, ArrayList<Director> directors) {
        this.genres    = genres;
        this.actors    = actors;
        this.directors = directors;

        JLabel titleLabel = new JLabel("Titre : ");
        JPanel titlePanel = new JPanel();
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.X_AXIS));
        titlePanel.add(titleLabel);
        titlePanel.add(titleEditor);

        JLabel synopsisLabel = new JLabel("Synopsis : ");
        JPanel synopsisPanel = new JPanel();
        synopsisEditor.setLineWrap(true);
        synopsisPanel.setLayout(new BoxLayout(synopsisPanel, BoxLayout.X_AXIS));
        synopsisPanel.add(synopsisLabel);
        synopsisPanel.add(synopsisEditor);

        JLabel runtimeLabel = new JLabel("Durée : ");
        JPanel runtimePanel = new JPanel();
        runtimePanel.setLayout(new BoxLayout(runtimePanel, BoxLayout.X_AXIS));
        runtimePanel.add(runtimeLabel);
        runtimePanel.add(runtimeEditor);

        JLabel posterLabel = new JLabel("Affiche : ");
        JPanel posterPanel = new JPanel();
        posterPanel.setLayout(new BoxLayout(posterPanel, BoxLayout.X_AXIS));
        posterPanel.add(posterLabel);
        posterPanel.add(posterEditor);

        JLabel genresLabel = new JLabel("Genres : ");
        JPanel genresPanel = new JPanel();
        genresEditor.setListData(genres.toArray());
        genresPanel.setLayout(new BoxLayout(genresPanel, BoxLayout.X_AXIS));
        genresPanel.add(genresLabel);
        genresPanel.add(genresEditor);

        ArrayList<String> actorsNames = new ArrayList<String>();
        for (Actor a : actors)
            actorsNames.add(a.getFirstname() + " " + a.getLastname());
        JLabel actorsLabel = new JLabel("Acteurs : ");
        JPanel actorsPanel = new JPanel();
        JButton addActor   = new JButton("+");
        actorsEditor.setListData(actorsNames.toArray());
        actorsPanel.setLayout(new BoxLayout(actorsPanel, BoxLayout.X_AXIS));
        actorsPanel.add(actorsLabel);
        actorsPanel.add(actorsEditor);
        actorsPanel.add(addActor);

        ArrayList<String> dirsNames = new ArrayList<String>();
        for (Director a : directors)
            dirsNames.add(a.getFirstname() + " " + a.getLastname());
        JLabel dirsLabel  = new JLabel("Réalisateur : ");
        JPanel dirsPanel  = new JPanel();
        JButton addDir    = new JButton("+");
        dirsEditor.setListData(dirsNames.toArray());
        dirsEditor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dirsPanel.setLayout(new BoxLayout(dirsPanel, BoxLayout.X_AXIS));
        dirsPanel.add(dirsLabel);
        dirsPanel.add(dirsEditor);
        dirsPanel.add(addDir);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(titlePanel);
        mainPanel.add(synopsisPanel);
        mainPanel.add(runtimePanel);
        mainPanel.add(posterPanel);
        mainPanel.add(genresPanel);
        mainPanel.add(actorsPanel);
        mainPanel.add(dirsPanel);

        add(mainPanel);
    }
}
