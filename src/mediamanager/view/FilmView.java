package mediamanager.view;

import fr.unice.polytech.mediamanager.model.Film;
import fr.unice.polytech.mediamanager.model.Actor;
import fr.unice.polytech.mediamanager.model.Genre;
import util.Util;
import util.MyLabel;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.Graphics2D;
import java.awt.AlphaComposite;
import java.awt.GradientPaint;
import java.awt.Transparency;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsConfiguration;

import java.util.ArrayList;
import java.io.File;

import java.lang.reflect.*;

abstract public class FilmView extends JPanel {
    // image de l'icône
    protected BufferedImage image;
    protected JLabel  imageLabel    = new JLabel();
    protected MyLabel titleLabel    = new MyLabel();
    protected MyLabel synopsisLabel = new MyLabel();
    protected MyLabel directorLabel = new MyLabel();
    protected MyLabel actorsLabel   = new MyLabel();
    protected MyLabel genresLabel   = new MyLabel();
    protected MyLabel runtimeLabel  = new MyLabel();

    protected Film model;

    public FilmView(Film m) {
        this.model = m;

        String imageFile = "resources/posters/unknownPoster.jpg";
        if ((new File(model.getPoster())).exists())
            imageFile = model.getPoster();

        try {
            image = ImageIO.read(new File(imageFile));
        } catch(java.io.IOException e) {
            e.printStackTrace();
        }
        imageLabel.setIcon(new ImageIcon(image));

        titleLabel.setText(model.getTitle());

        synopsisLabel.setText(model.getSynopsis());

        directorLabel.setText(model.getDirector().getFirstname() + " " + model.getDirector().getLastname());
        
        runtimeLabel.setText(""+model.getRuntime());

        ArrayList<String> actors = new ArrayList<String>();
        for (Actor a : model.getActors())
            actors.add(a.getFirstname() + " " + a.getLastname());
        actorsLabel.setText(Util.join(actors, ", "));

        ArrayList<String> genres = new ArrayList<String>();
        for (Genre a : model.getGenres())
            genres.add(a.getLabelFr());
        genresLabel.setText(Util.join(genres, ", "));

        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    ((MediaManagerView) getParent().getParent().getParent().getParent().getParent().getParent().getParent().getParent().getParent().getParent()).openAddFilm(model); // TODO
                }
            }
        });
    }

    public void setVisible(String label, boolean isVisible) {
        String dataName = label.toLowerCase() + "Label";
        Class<?> c = FilmView.class;

        try {
            Field field = c.getDeclaredField(dataName);
            Object valueField = field.get(this);
            Class<?> classField = valueField.getClass();
            Method method = classField.getMethod("setVisible", new Class[] { boolean.class });
            method.invoke(valueField, isVisible);
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        } catch (java.lang.NoSuchFieldException e) {
            e.printStackTrace();
        } catch (java.lang.NoSuchMethodException e) {
            e.printStackTrace();
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected static BufferedImage resize(BufferedImage bi, double scaleValue) {
        if (scaleValue <= 0) scaleValue = 1;

        AffineTransform tx = new AffineTransform();
        tx.scale(scaleValue, scaleValue);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        BufferedImage biNew = new BufferedImage((int) (bi.getWidth() * scaleValue),
                                                (int) (bi.getHeight() * scaleValue),
                                                bi.getType());
        return op.filter(bi, biNew);

    }

    protected static Image reflection(Image image) {
        image = resize((BufferedImage) image, 0.5);

        int imageWidth  = image.getWidth(null);
        int imageHeight = image.getHeight(null);
        int width  = imageWidth;
        int height = imageHeight * 2;
        int gap = 0;
        float opacity = 0.4f;
        float fadeHeight = 0.3f;


        GraphicsEnvironment   ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice        gs = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gs.getDefaultConfiguration();

        BufferedImage returnImage = gc.createCompatibleImage(width, height, Transparency.TRANSLUCENT);
        Graphics2D g2d = returnImage.createGraphics();

        g2d.setPaint(new GradientPaint(0, 0, Color.black, 0, height, Color.darkGray));
        g2d.fillRect(0, 0, width, height);
        g2d.translate((width - imageWidth) / 2, height / 2 - imageHeight);
        g2d.drawRenderedImage((BufferedImage)image, null);
        g2d.translate(0, 2 * imageHeight + gap);
        g2d.scale(1, -1);

        BufferedImage reflection = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D rg = reflection.createGraphics();
        rg.drawRenderedImage((BufferedImage)image, null);
        rg.setComposite(AlphaComposite.getInstance( AlphaComposite.DST_IN ));
        rg.setPaint( 
                new GradientPaint( 
                    0, imageHeight*fadeHeight, new Color( 0.0f, 0.0f, 0.0f, 0.0f ),
                    0, imageHeight, new Color( 0.0f, 0.0f, 0.0f, opacity )
                    )
                );
        rg.fillRect( 0, 0, imageWidth, imageHeight );
        rg.dispose();
        g2d.drawRenderedImage(reflection, null);
        g2d.dispose();

        return returnImage;
    }

    public String toString() {
        return model.getTitle();
    }
}
