package mediamanager.view;

import fr.unice.polytech.mediamanager.model.Film;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.BorderFactory;

import java.awt.Font;

public class FilmMediumView extends FilmView {
    public static final int IMG_WIDTH = 220;

    public FilmMediumView(Film model) {
        super(model);

        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD).deriveFont(25f));

        double ratio = (double) IMG_WIDTH / image.getWidth();
        imageLabel.setIcon(new ImageIcon(resize(image, ratio)));

        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.add(synopsisLabel);
        infoPanel.add(directorLabel);
        infoPanel.add(runtimeLabel);
        infoPanel.add(actorsLabel);
        infoPanel.add(genresLabel);
        infoPanel.add(Box.createVerticalGlue());

        JPanel imgPanel = new JPanel();
        imgPanel.setLayout(new BoxLayout(imgPanel, BoxLayout.X_AXIS));
        imgPanel.add(imageLabel);
        imgPanel.add(infoPanel);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(JPanel.LEFT_ALIGNMENT);
        add(titleLabel);
        add(imgPanel);
    }
}
