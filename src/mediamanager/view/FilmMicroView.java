package mediamanager.view;

import fr.unice.polytech.mediamanager.model.Film;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;

public class FilmMicroView extends FilmView {
    public static final int WIDTH_MIN  = 300,
                            WIDTH_MAX  = WIDTH_MIN * 2,
                            HEIGHT_MIN = 200,
                            HEIGHT_MAX = HEIGHT_MIN * 2;

    private static final int TITLE_SIZE_MAX = 20,
                             SYNOPSIS_SIZE_MAX = 20;
    private Color  bgColor;
    private JPanel infoPanel = new JPanel();

    public FilmMicroView(Film model) {
        super(model);

        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD).deriveFont(18f));

        infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.add(titleLabel);
        infoPanel.add(synopsisLabel);
        infoPanel.add(directorLabel);
        infoPanel.add(runtimeLabel);
        infoPanel.add(actorsLabel);
        infoPanel.add(genresLabel);
        infoPanel.add(Box.createVerticalGlue());

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        setAlignmentX(JPanel.LEFT_ALIGNMENT);
        add(imageLabel);
        add(infoPanel);


        bgColor = getBackground();

        addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                //setBackground(Color.WHITE);
                //infoPanel.setBackground(Color.WHITE);
            }
            public void mouseExited(MouseEvent e) {
                //setBackground(bgColor);
                //infoPanel.setBackground(bgColor);
            }
        });

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                if (e.getID() == ComponentEvent.COMPONENT_RESIZED) {
                    Dimension size = getParent().getParent().getParent().getParent().getSize();

                    int nbMin = size.width / WIDTH_MIN;
                    int nbMax = size.width / WIDTH_MAX;
                    int nb = 1;
                    if (nbMin > nbMax)
                        nb = nbMin;
                    else
                        nb = nbMax;
                    if (nb <= 0) nb = 1;

                    double ratio = ((double) size.width / nb) / WIDTH_MIN;
                    imageLabel.setIcon(new ImageIcon(resize(image, ratio)));
                    //imageLabel.setIcon(new ImageIcon(reflection(resize(image, ratio))));
                    setPreferredSize(new Dimension(getPreferredSize().width, imageLabel.getIcon().getIconHeight()));
                }
            }
        });

        setPreferredSize(new Dimension(WIDTH_MIN, HEIGHT_MIN));
        setMinimumSize(new Dimension(WIDTH_MIN, HEIGHT_MIN));
        setMaximumSize(new Dimension(WIDTH_MAX, HEIGHT_MAX));
    }
}
