package mediamanager.view;

import fr.unice.polytech.mediamanager.model.Film;
import mediamanager.controller.Controller;
import mediamanager.event.*;
import util.MyLabel;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

import java.awt.Font;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import java.util.HashMap;

import java.lang.reflect.*;

public class FilmMacroView extends FilmView {
    public static final int IMG_WIDTH = 500;

    protected Controller controller;
    protected JPopupMenu popup = new JPopupMenu();

    public FilmMacroView(Film m, Controller c) {
        super(m);
        controller = c;

        double ratio = (double) IMG_WIDTH / image.getWidth();
        imageLabel.setIcon(new ImageIcon(reflection(resize(image, ratio))));

        JMenuItem menuItem = new JMenuItem("Supprimer");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventFilmDelete event = new EventFilmDelete(this, model);
                controller.filmChanged(event);
            }
        });
        popup.add(menuItem);
        addMouseListener(new PopupListener());

        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD).deriveFont(25f));

        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.add(createEditor("synopsis"));
        infoPanel.add(createEditor("director"));
        infoPanel.add(createEditor("runtime"));
        infoPanel.add(actorsLabel);
        infoPanel.add(genresLabel);
        infoPanel.add(Box.createVerticalGlue());

        JPanel imgPanel = new JPanel();
        imgPanel.setLayout(new BoxLayout(imgPanel, BoxLayout.X_AXIS));
        imgPanel.add(imageLabel);
        imgPanel.add(infoPanel);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(JPanel.LEFT_ALIGNMENT);
        add(createEditor("title"));
        add(imgPanel);
    }

    private class ListenerEditor extends MouseAdapter implements ActionListener, FocusListener {
        private JPanel     panel;
        private JTextField editor;
        private MyLabel    label;
        private String     data;

        public ListenerEditor(JPanel panel, JTextField editor, MyLabel label, String data) {
            this.panel  = panel;
            this.editor = editor;
            this.label  = label;
            this.data   = data;
        }

        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                CardLayout cl = (CardLayout)(panel.getLayout());
                cl.show(panel, "edit");
                editor.requestFocus();
            }
        }

        public void actionPerformed(ActionEvent e) {
            updateModel();
        }

        public void focusLost(FocusEvent e) {
            updateModel();
        }

        public void focusGained(FocusEvent e) {
        }

        private void updateModel() {
            CardLayout cl = (CardLayout)(panel.getLayout());
            cl.show(panel, "view");
            String returnString = editor.getText();
            if (!returnString.equals(label.getText())) {
                EventFilmUpdate event = new EventFilmUpdate(this, model, data, returnString);
                controller.filmChanged(event);
            }
        }
    }

    public JPanel createEditor(String dataName) {
        MyLabel label = null;
        Class<?> c = FilmView.class;
        try {
            Field field   = c.getDeclaredField(dataName + "Label");
            label = (MyLabel) field.get(this);
        } catch (java.lang.NoSuchFieldException e) {
            e.printStackTrace();
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
        }

        JTextField editor = new JTextField();
        editor.setFont(label.getFont());
        editor.setText(label.getText());

        JPanel panel = new JPanel();
        panel.setLayout(new CardLayout());
        panel.add(label,  "view");
        panel.add(editor, "edit");

        ListenerEditor listener = new ListenerEditor(panel, editor, label, dataName);
        label.addMouseListener(listener);
        editor.addActionListener(listener);
        editor.addFocusListener(listener);

        return panel;
    }

    protected class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }
}
