package mediamanager.controller;

import mediamanager.view.MediaManagerView;
import mediamanager.model.Manager;
import mediamanager.event.*;
import mediamanager.search.MediaManagerSearchInput;
import util.Util;

import fr.unice.polytech.mediamanager.model.Film;
import fr.unice.polytech.mediamanager.model.Actor;
import fr.unice.polytech.mediamanager.model.Director;
import fr.unice.polytech.mediamanager.model.Genre;
import fr.unice.polytech.mediamanager.model.Nationality;

import mediamanager.search.MediaManagerSearchController;
import mediamanager.search.MediaManagerSearchInput;
import search.model.ElementaryCondition;
import mock.Item;
import java.util.GregorianCalendar;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;

import java.lang.reflect.*;

public class Controller {
    private Manager          model;
    private MediaManagerView view;
	private MediaManagerSearchController search;

    private final static HashMap<String, Class> dataType = new HashMap<String, Class>();
    static {
        dataType.put("title", String.class);
        dataType.put("synopsis", String.class);
        dataType.put("director", Director.class);
        dataType.put("runtime", int.class);
        dataType.put("poster", String.class);
    }

    public Controller(Manager model) {
        this.model = model;
		search = new MediaManagerSearchController(new MediaManagerSearchInput());
        this.view  = new MediaManagerView(this, model, search.getView());
		search.addSearchListener(this.view);
		search.addManagerListener(this.view);
		search.evaluate();
    }
	
    public boolean addFilm(Film f) {
        return model.addFilm(f);
    }

    public boolean updateFilm(Film f) {
        return model.updateFilm(f);
    }

    public boolean filmChanged(EventFilmUpdate f) {
        String methodName = "set" + Util.capitalize(f.getData());
        Class<?> c = Film.class;

        try {
            Method method = c.getMethod(methodName, new Class[] { dataType.get(f.getData()) });
            method.invoke(f.getModel(), f.getNewValue());
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        } catch (java.lang.NoSuchMethodException e) {
            e.printStackTrace();
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
        }
        return model.updateFilm(f.getModel());
    }

    public boolean filmChanged(EventFilmDelete f) {
        return model.deleteFilm(f.getModel());
    }

    public boolean addNewFilm() {
        ArrayList<Genre> genres = new ArrayList<Genre>();
        ArrayList<Actor> actors = new ArrayList<Actor>();

        Director director = new Director("", "inconnu", "", Nationality.french, new GregorianCalendar(0+1900, 0, 0).getTime(), new GregorianCalendar(0+1900,0,0).getTime(), "");

        Film film = new Film("", "inconnu", director, actors, genres, 0, "", "");

        return model.addFilm(film);
    }
}
