package mediamanager;

import mediamanager.model.SingletonManager;
import mediamanager.controller.Controller;

import javax.swing.UIManager;
import javax.swing.SwingUtilities;

import org.jvnet.substance.skin.SubstanceRavenGraphiteLookAndFeel;

public class MediaManager {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new SubstanceRavenGraphiteLookAndFeel());
        } catch (javax.swing.UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Controller(SingletonManager.getManager());
            }
        });
    }
}
