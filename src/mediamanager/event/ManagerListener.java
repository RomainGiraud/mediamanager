package mediamanager.event;

import fr.unice.polytech.mediamanager.model.Film;

public interface ManagerListener {
    public void filmAdded(Film film);
    public void filmDeleted(Film film);
    public void filmUpdated(Film film);
}
