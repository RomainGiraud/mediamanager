package mediamanager.event;

import fr.unice.polytech.mediamanager.model.Film;

import java.util.EventObject;

public abstract class EventFilm extends EventObject {
    private Film model;

    public EventFilm(Object source, Film model) {
        super(source);
        this.model = model;
    }

    public Film getModel() {
        return model;
    }
}
