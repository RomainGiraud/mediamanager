package mediamanager.event;

import fr.unice.polytech.mediamanager.model.Film;

public class EventFilmDelete extends EventFilm {
    public EventFilmDelete(Object source, Film model) {
        super(source, model);
    }
}
