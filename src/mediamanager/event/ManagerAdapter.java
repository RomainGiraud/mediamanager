package mediamanager.event;

import fr.unice.polytech.mediamanager.model.Film;

public class ManagerAdapter implements ManagerListener {
    public void filmAdded(Film film) {}
    public void filmDeleted(Film film) {}
    public void filmUpdated(Film film) {}
}
