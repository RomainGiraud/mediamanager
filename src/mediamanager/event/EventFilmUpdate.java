package mediamanager.event;

import fr.unice.polytech.mediamanager.model.Film;

public class EventFilmUpdate extends EventFilm {
    private String data;
    private String newValue;

    public EventFilmUpdate(Object source, Film model, String data, String newValue) {
        super(source, model);
        this.data     = data;
        this.newValue = newValue;
    }

    public String getData() {
        return data;
    }

    public String getNewValue() {
        return newValue;
    }
}
