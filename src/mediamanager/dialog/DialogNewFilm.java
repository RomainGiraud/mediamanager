package mediamanager.dialog;

import fr.unice.polytech.mediamanager.model.*;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class DialogNewFilm extends JDialog {
    public DialogNewFilm(ArrayList<Genre> genres, ArrayList<Actor> actors, ArrayList<Director> directors) {
        setTitle("Ajouter un film");

        JLabel     titleLabel  = new JLabel("Titre : ");
        JTextField titleEditor = new JTextField();
        JPanel     titlePanel  = new JPanel();
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.X_AXIS));
        titlePanel.add(titleLabel);
        titlePanel.add(titleEditor);

        JLabel     synopsisLabel  = new JLabel("Synopsis : ");
        JTextField synopsisEditor = new JTextField();
        JPanel     synopsisPanel  = new JPanel();
        synopsisPanel.setLayout(new BoxLayout(synopsisPanel, BoxLayout.X_AXIS));
        synopsisPanel.add(synopsisLabel);
        synopsisPanel.add(synopsisEditor);

        JLabel       runtimeLabel  = new JLabel("Durée : ");
        SpinnerModel runtimeModelEditor = new SpinnerNumberModel();
        JSpinner     runtimeEditor = new JSpinner(runtimeModelEditor);
        JPanel       runtimePanel  = new JPanel();
        runtimePanel.setLayout(new BoxLayout(runtimePanel, BoxLayout.X_AXIS));
        runtimePanel.add(runtimeLabel);
        runtimePanel.add(runtimeEditor);

        JLabel     posterLabel  = new JLabel("Poster : ");
        JTextField posterEditor = new JTextField();
        JPanel     posterPanel  = new JPanel();
        posterPanel.setLayout(new BoxLayout(posterPanel, BoxLayout.X_AXIS));
        posterPanel.add(posterLabel);
        posterPanel.add(posterEditor);

        JLabel genreLabel  = new JLabel("Genres : ");
        JList  genreEditor = new JList(genres.toArray());
        JPanel genrePanel  = new JPanel();
        genrePanel.setLayout(new BoxLayout(genrePanel, BoxLayout.X_AXIS));
        genrePanel.add(genreLabel);
        genrePanel.add(genreEditor);

        ArrayList<String> actorsNames = new ArrayList<String>();
        for (Actor a : actors)
            actorsNames.add(a.getFirstname() + " " + a.getLastname());
        JLabel actorLabel  = new JLabel("Acteurs : ");
        JList  actorEditor = new JList(actorsNames.toArray());
        JPanel actorPanel  = new JPanel();
        JButton addActor = new JButton("+");
        actorPanel.setLayout(new BoxLayout(actorPanel, BoxLayout.X_AXIS));
        actorPanel.add(actorLabel);
        actorPanel.add(actorEditor);
        actorPanel.add(addActor);

        ArrayList<String> dirsNames = new ArrayList<String>();
        for (Director a : directors)
            dirsNames.add(a.getFirstname() + " " + a.getLastname());
        JLabel dirLabel  = new JLabel("Réalisateur : ");
        JList  dirEditor = new JList(dirsNames.toArray());
        dirEditor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JPanel dirPanel  = new JPanel();
        JButton addDir = new JButton("+");
        dirPanel.setLayout(new BoxLayout(dirPanel, BoxLayout.X_AXIS));
        dirPanel.add(dirLabel);
        dirPanel.add(dirEditor);
        dirPanel.add(addDir);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(titlePanel);
        mainPanel.add(synopsisPanel);
        mainPanel.add(runtimePanel);
        mainPanel.add(posterPanel);
        mainPanel.add(genrePanel);
        mainPanel.add(actorPanel);
        mainPanel.add(dirPanel);

        add(mainPanel);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
