package mediamanager.model;

public class SingletonManager {
    private SingletonManager() {}

    static private Manager model = new Manager();

    public static Manager getManager() {
        return model;
    }
}
