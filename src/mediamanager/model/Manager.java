package mediamanager.model;

import mediamanager.event.ManagerListener;

import fr.unice.polytech.mediamanager.model.Film;

import java.util.ArrayList;

public class Manager extends fr.unice.polytech.mediamanager.model.Manager {
    private ArrayList<ManagerListener> listManagerListener = new ArrayList<ManagerListener>();

    public void addManagerListener(ManagerListener m) {
        listManagerListener.add(m);
    }

    public void removeManagerListener(ManagerListener m) {
        listManagerListener.remove(m);
    }

    private void fireEventFilmAdded(Film film) {
        for (ManagerListener m : listManagerListener)
            m.filmAdded(film);
    }

    private void fireEventFilmDeleted(Film film) {
        for (ManagerListener m : listManagerListener)
            m.filmDeleted(film);
    }

    private void fireEventFilmUpdated(Film film) {
        for (ManagerListener m : listManagerListener)
            m.filmUpdated(film);
    }

    public boolean addFilm(Film film) {
        if (super.addFilm(film)) {
            fireEventFilmAdded(film);
            return true;
        }
        return false;
    }

    public boolean deleteFilm(Film film) {
        if (super.deleteFilm(film)) {
            fireEventFilmDeleted(film);
            return true;
        }
        return false;
    }

    public boolean updateFilm(Film film) {
        if (super.updateFilm(film)) {
            fireEventFilmUpdated(film);
            return true;
        }
        return false;
    }
}
