package mediamanager.search;

import fr.unice.polytech.mediamanager.model.Film;

import mediamanager.event.ManagerListener;

import java.util.List;
import java.util.LinkedList;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class MediaManagerSearchController extends search.controller.SearchController<Film> implements ManagerListener {
    private List<ManagerListener> listManagerListener = new LinkedList<ManagerListener>();
		
	public MediaManagerSearchController(search.client.SearchInputProvider<Film> input) {
		super(input);
	}
	
    public void filmAdded(Film film) {
		if (evaluate(film))
			fireEventFilmAdded(film);
	}
    public void filmDeleted(Film film) {
		fireEventFilmDeleted(film);
	}
    public void filmUpdated(Film film) {
		if (evaluate(film))
			fireEventFilmUpdated(film);
		else
			fireEventFilmDeleted(film);
	}
	
    public void addManagerListener(ManagerListener m) {
        listManagerListener.add(m);
    }
	
    public void removeManagerListener(ManagerListener m) {
        listManagerListener.remove(m);
    }
	
    private void fireEventFilmAdded(Film film) {
        for (ManagerListener m : listManagerListener)
            m.filmAdded(film);
    }
	
    private void fireEventFilmDeleted(Film film) {
        for (ManagerListener m : listManagerListener)
            m.filmDeleted(film);
    }
	
    private void fireEventFilmUpdated(Film film) {
        for (ManagerListener m : listManagerListener)
            m.filmUpdated(film);
    }

}