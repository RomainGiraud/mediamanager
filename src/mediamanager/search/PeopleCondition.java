package search.sample;

import java.util.List;
import java.util.ArrayList;

import search.model.ElementaryCondition;

import fr.unice.polytech.mediamanager.model.People;


/**An ElementaryCondition that evaluates textual conditions.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public abstract class PeopleCondition<T> extends TextualCondition<T> {
	private enum Name {
		FIRST, LAST;
	}
	
	protected enum PeopleAction {
		FIRST_NAME_CONTAINS("a un prénom contenant", TextAction.CONTAINS, Name.FIRST),
		FIRST_NAME_CONTAINS_NOT("a un prénom ne contenant pas", TextAction.CONTAINS_NOT, Name.FIRST),
		FIRST_NAME_IS("a un prénom étant", TextAction.IS, Name.FIRST),
		FIRST_NAME_IS_NOT("a un prénom n'étant pas", TextAction.IS_NOT, Name.FIRST),
		LAST_NAME_CONTAINS("a un nom de famille contenant", TextAction.CONTAINS, Name.LAST),
		LAST_NAME_CONTAINS_NOT("a un nom de famille ne contenant pas", TextAction.CONTAINS_NOT, Name.LAST),
		LAST_NAME_IS("a un nom de famille étant", TextAction.IS, Name.LAST),
		LAST_NAME_IS_NOT("a un nom de famille n'étant pas", TextAction.IS_NOT, Name.LAST);
		
		private String name;
		private TextAction action;
		private Name nameType;
		
		PeopleAction(String name, TextAction action, Name nameType) {
			this.name = name;
			this.action = action;
			this.nameType = nameType;
		}
		
		public String label() {
			return name;
		}
		
		public TextAction textAction() {
			return action;
		}
		
		public Name nameType() {
			return nameType;
		}
		
		protected static List<String> labels() {
			List<String> result = new ArrayList<String>(values().length);
			for (PeopleAction a : values())
				result.add(a.label());
			return result;
		}
	}
	
	protected PeopleCondition() {
		super(PeopleAction.labels());
	}
	
	protected PeopleCondition(List<String> notUsed) {
		super(PeopleAction.labels());
	}
	
	public Boolean eval(People f) {
		for (PeopleAction act : PeopleAction.values())
			if (selectedAction().equals(act.label()))
				return super.eval((act.nameType == Name.FIRST ? f.getFirstname() : f.getLastname()), act.textAction().label());
		throw new IllegalArgumentException("The given action isn't allowed !");
	}
}