package mediamanager.search;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import search.client.SearchInputProvider;
import search.model.ElementaryCondition;
import search.sample.TextualCondition;
import search.model.PrintTypes;

import mediamanager.model.SingletonManager;

import fr.unice.polytech.mediamanager.model.Film;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class MediaManagerSearchInput implements SearchInputProvider<Film> {
	private static Map<String, ElementaryCondition<Film>> map = null;
	
	public Map<String, ElementaryCondition<Film>> getConditions() {
		if (map != null)
			return map;
		
		map = new HashMap<String, ElementaryCondition<Film>>();
		map.put("Le synopsis", new TextualCondition<Film>() {
				   public Boolean eval(Film f) {
						return eval(f.getSynopsis());
				   }
				   
				   protected String prefix(PrintTypes type) {
						if (type == PrintTypes.BOOLEAN)
							return "synopsis";
					   return "Le synopsis";
				   }
			   }
	     );
		map.put("Le titre", new TextualCondition<Film>() {
				public Boolean eval(Film f) {
				return eval(f.getTitle());
				}
				
				public String prefix(PrintTypes type) {
				if (type == PrintTypes.BOOLEAN)
				return "titre";
				return "Le titre";
				}
				}
				);
		return map;
	}
	
	public List<Film> getData() {
		return SingletonManager.getManager().getAllFilms();
	}
}