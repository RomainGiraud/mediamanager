package util;

import javax.swing.JTextField;
import javax.swing.UIManager;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;

public class MyLabel extends JTextField {
    private String label = "";
    private int    width = 0;
    private int    size  = 0;

    public MyLabel() {
        this("");
    }

    public MyLabel(String text) {
        super(text);

        setEditable(false);
        setBorder(null);
        setForeground(UIManager.getColor("Label.foreground"));
        setBackground(UIManager.getColor("Label.background"));
        setFont(UIManager.getFont("Label.font"));
        setOpaque(true);

        setText(label);

        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent e) {
                //calculate();
            }
            public void componentResized(ComponentEvent e) {
                calculate();
            }
            private void calculate() {
                Rectangle rect = new Rectangle();
                computeVisibleRect(rect);
                int widthDisplay = (int) rect.width;
                if (width >= widthDisplay && width != 0) {
                    int c = size * widthDisplay / width;
                    if (c >= 3) c -= 3;
                    setTextFromListener(label.substring(0, c) + "...");
                } else {
                    setTextFromListener(label);
                }
            }
        });
    }

    public void setTextFromListener(String text) {
        super.setText(text);
    }

    public String getText() {
        return label;
    }

    private void recalculate() {
        if (label == null) return;
        size  = label.length();
        width = getFontMetrics(getFont()).stringWidth(label);
    }

    public void setText(String text) {
        super.setText(text);
        label = text;
        recalculate();
    }

    public void setFont(Font font) {
        super.setFont(font);
        recalculate();
        setMaximumSize(new Dimension((int)getMaximumSize().getWidth(), getFontMetrics(getFont()).getHeight()));
    }
}
