package search.model;

/**These types define the different ways of printing a condition.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public enum PrintTypes {
	SPOKEN_LANGUAGE("Texte"),
	BOOLEAN("Booléen");

	private final String name;

	private PrintTypes(String name) {
		this.name = name;
	}

	/**Returns the name of the type.
	 */
	public String toString() {
		return name;
	}
}