package search.model;

import search.event.*;

/**A Condition object allows one to evaluate abritrary tests on objects.
 *It is actually an abstraction of a formal logic predicate.
 *This class implements Cloneable because every condition has to be cloneable. Make sure you don't mess up with pointers being duplicated, you have to actually <em>clone</em> the object, or everything will fail !
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.2
 */

public abstract class Condition<T> extends ConditionEventsSource<T> implements Cloneable {
	public abstract Boolean evaluate(T o);
	public abstract String toString(PrintTypes type);
		
	/**Publicly-visible remove method, with events bubbling management.
	 */
	public void remove() {
		remove(this);
	}
	
	protected void remove(Condition<T> what) {
		if (parent() != null)
			parent().remove(what);
		doRemove(what);
		notify(new ConditionRemovedEvent(what));
	}
	
	/**The actual work of removing the condition is up to your implmentation.
	 */
	protected abstract void doRemove(Condition<T> what);
	
	/**<strong>To be overriden !!!</strong>
	 *@throws	RuntimeException	instead of CloneNotSupportedException
	 */
	@SuppressWarnings("unchecked")
	public Condition<T> clone() {
		Condition<T> result;
		try {
			result = (Condition<T>) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		result.initListeners();
		return result;
	}
}