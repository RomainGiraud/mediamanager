package search.model;

import java.util.Map;
import java.util.HashMap;

/**These operators model boolean operations.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public enum Operators {
	AND(andMap()),
	OR(orMap());

	private Map<PrintTypes, String> map;

	private Operators(Map<PrintTypes, String> map) {
		this.map = map;
	}

	/**Returns a readable (spoken-language) form of the operator.
	 */
	public String toString() {
		return toString(PrintTypes.SPOKEN_LANGUAGE);
	}
	
	public String toString(PrintTypes type) {
		return map.get(type);
	}
	
	private static Map<PrintTypes, String> andMap() {
		Map<PrintTypes, String> result = new HashMap<PrintTypes, String>();
		result.put(PrintTypes.SPOKEN_LANGUAGE, "et");
		result.put(PrintTypes.BOOLEAN, ".");
		return result;
	}
	private static Map<PrintTypes, String> orMap() {
		Map<PrintTypes, String> result = new HashMap<PrintTypes, String>();
		result.put(PrintTypes.SPOKEN_LANGUAGE, "ou");
		result.put(PrintTypes.BOOLEAN, "+");
		return result;
	}
}