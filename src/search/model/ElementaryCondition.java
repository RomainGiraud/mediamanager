package search.model;

import java.lang.reflect.Method;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import search.event.*;

/**A Condition object allows one to evaluate abritrary tests on objects.
 *It is highly recommended that you also override the toString and toSymbols methods, in order to have a correct view display.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public abstract class ElementaryCondition<T> extends Condition<T> {	
	/**Stores every action that may be used upon a T to extract necessary data for testing.
	 *Keys are labels for the action. These may then be used inside the eval method, to differentiate treatment.
	 *@see	eval
	 */
	private List<String> availableActions;
	/**Stored operand for the coming tests.
	 */
	private Object operand;
	/**Stores the key of the selected action.
	 *@see	availableActions
	 */
	private String selectedAction;
	
	/*Screw Java : http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4071957 : great modular design destroyed by a f*cking 12-year-old bug !!!!
	 *Problem is a public method isn't accessible if its enclosing class in not accessible (i.e. in another package), even though it SHOULD be, being public.
	 *Since this module was designed for use through instanciation overriding, we can't use this.
	 * Following code stays in, hoping one day Sun will fix this.
	 * =====[TOFIX (BY SUN)]=====
	 */
	/**The name of the method to be declared at instanciation by new classes.
	 *Using this trick allows one to determine the method to use at runtime, through reflection. This allows one to avoid having an abstract method with an Object parameter, forcing a cast. You may use the type of parameter you wish, and have differently-typed methods.
	 *The evaluation method must have this name, and have one and only one parameter, which will be the result of the evaluation of the selected action on the object this condition will be evaluated on.
	 */
	/*public final static String EVAL_METHOD_NAME = "eval";*/
	
	
	public ElementaryCondition() {
		 this(new LinkedList<String>());
	}
	
	/**
	 */
	 public ElementaryCondition(List<String> availableActions) {
		 operand = "";
		 this.availableActions = availableActions;
		 if (availableActions.size() > 0)
			 selectAction(availableActions.get(0));
/*	=====[TOFIX]=====
	This would be used to check that there is at least one evaluation method available, since determining it at runtime deactivates all compiler checks.
 
		try {
			this.getClass().getMethod(EVAL_METHOD_NAME, Object.class);
		 } catch (java.lang.NoSuchMethodException e) {
			throw new RuntimeException("No implementation found for any evaluation method !\nRemember that their name must be " + EVAL_METHOD_NAME + " and they must have one argument. See ElementaryCondition for more details.");
		 }
 	 =====[/TOFIX]=====
*/
	 }
	
	
	/**@name	Getters*/
	//@{	
	public Object operand() {
		return operand;
	}
	
	public String selectedAction() {
		return selectedAction;
	}
	
	public List<String> availableActions() {
		return availableActions;
	}
	//@}
	
	
	/**@name	Setters*/
	//@{
	/**Sets the operand variable to the given value, which may later be accessed in the evaluate method.
	 */
	public ElementaryCondition<T> setOperand(Object operand) {
		this.operand = operand;
		notify(new ConditionUpdatedEvent(this, ConditionUpdatedEvent.Field.OPERAND, operand));
		return this;
	}
	
	/**Selects the given action from its index in the list of available actions.
	 */
	public ElementaryCondition<T> selectAction(int index) {
		return selectAction(availableActions.get(index));
	}
	
	/**Selects the given action in the list of available actions.
	 *If this action is not already in the list, it will be added to it.
	 *TODO : current implementation is not list-empty-safe !
	 */
	public ElementaryCondition<T> selectAction(String key) {
		if (key == null || key.trim().isEmpty())
			return this;
		if (! availableActions.contains(key))
			availableActions.add(key);
		this.selectedAction = key;
		notify(new ConditionUpdatedEvent(this, ConditionUpdatedEvent.Field.ACTION, key));
		return this;
	}
	
	public ElementaryCondition<T> setActions(List<String> labels) {
		availableActions = labels;
		return this;
	}	
	
	public ElementaryCondition<T> addAction(String label) {
		availableActions.add(label);
		if (availableActions.size() == 1)
			selectAction(label);
		return this;
	}
	
	/**Removes the given action from the list of available actions, and selects the previous one if the one removed was the selected one.
	 */
	public ElementaryCondition<T> removeAction(String label) {
		int index = availableActions.indexOf(label);
		availableActions.remove(index);
		
		if (availableActions.size() == 0) {
			selectedAction = null;
		} else {
			if (index == 0) //If the removed action was the first one of the list
				index++; //the next to be selected is the second (new first) one
			else
				index--; //else, we go up
			selectAction(availableActions.get(index));
		}
		return this;
	}
	//@}
	
	
	/**@name	Evaluation*/
	//@{
	/**In case of an internal failure (reflection…), returns false.
	 */
	public Boolean evaluate(T subject) {
		/*=====[TOFIX (BY SUN)]======
		try {
			Method toUse = this.getClass().getMethod(EVAL_METHOD_NAME, subject.getClass());
			return (Boolean) toUse.invoke(this, subject);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (java.lang.reflect.InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		 ======[/TOFIX]======
		 */
		return eval(subject);
	}
	
	/*=====[TOFIX (BY SUN)]=====
	 This method may be removed once Sun fixes the public-access reflection bug.
	 */
	/**Override at instanciation for the test you want to.
	 *Remember you can access the operand in this method, through the operand() method, and the selected action, through selectedAction().
	 *@param	subject	the object to be tested.
	 *@param	operand	the operand for the test.
	 *@see	setOperand
	 *@see	operand
	 *@see	selectedAction
	 */
	public abstract Boolean eval(T subject);
	/*======[/TOFIX]======*/
	//@}
	
	
	/**@name	Interface compliance*/
	//@{
	protected void doRemove(Condition<T> what) {
		if (what == this)
			try {
				finalize();
			} catch (Throwable t) {
				System.err.println("Error while trying to finalize condition : " + t);
			}
	}
	
	public String toString(PrintTypes type) {
		switch (type) {
			case BOOLEAN:
				return toSymbols();
			case SPOKEN_LANGUAGE:
			default:
				return toString();
		}
	}
	
	public String toString() {
		return prefix(PrintTypes.SPOKEN_LANGUAGE) + " " + (selectedAction != null ? selectedAction : " action liée à ") + " \"" + (operand != null ? operand.toString() : "")+ "\"";
	}
	
	public String toSymbols() {
		return  prefix(PrintTypes.BOOLEAN) + "->" + (selectedAction != null ? selectedAction.replace(" ", "_") : "indéfini") + "(" + (operand != null ? operand.toString() : "") + ")";
	}
	
	/**Will be appended to the beginning of the toString methods results.
	 */
	protected abstract String prefix(PrintTypes type);
	
	@SuppressWarnings("unchecked")
	public ElementaryCondition<T> clone() {
		ElementaryCondition<T> result;
		result = (ElementaryCondition<T>) super.clone();
		result.setActions(new ArrayList<String>(availableActions()));
		result.selectAction(selectedAction());
		result.setOperand(operand());
		return result;
	}
	//@}
}