package search.model;


import java.util.Iterator;

import java.util.List;
import java.util.LinkedList;

import search.event.*;


/**A ConditionGroup is a Condition itself, which allows for nested conditions.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class ConditionGroup<T> extends Condition<T> implements Iterable<Condition<T>> {
	public static final Boolean EMPTY_VALUE = Boolean.TRUE;
	private List<Condition<T>> conditions;
	private Operators link;
	
	public ConditionGroup() {
		this(Operators.AND);
	}
	
	public ConditionGroup(Operators link) {
		this.link = link;
		conditions = new LinkedList<Condition<T>>();
	}
	
	/**@name	Getters*/
	//@{
	public Operators operator() {
		return link;
	}
	
	public Boolean contains(Condition<T> cond) {
		return conditions.contains(cond);
	}
	
	public int size() {
		return conditions.size();
	}
	//@}
	
	
	/**@name	Setters*/
	//@{
	public ConditionGroup<T> setOperator(Operators link) {
		this.link = link;
		notify(new ConditionUpdatedEvent(this, ConditionUpdatedEvent.Field.OPERATOR, link));
		return this;
	}
	
	public ConditionGroup<T> add(Condition<T> c) {
		return addAfter(c, c);
	}
	
	public ConditionGroup<T> addAfter(Condition<T> what, Condition<T> where) {
		if (what == null)
			throw new IllegalArgumentException("Can't add a null condition !");
		int index;
		if (where == null) { //insert at top
			where = what;
			index = -1;
		} else {
			index = conditions.indexOf(where);
			if (index == -1) {
				index = conditions.size() - 1;
				where = what; //if the added condition is the only one available, by convention, the condition is said to have been added after itself
			}
		}
		conditions.add(index + 1, what);
		what.setParent(this);
		notify(new ConditionAddedEvent(what, where));
		return this;
	}
	
	/**If the given replaced item can't be found, returns false and does nothing.
	 */
	public boolean replace(Condition<T> from, Condition<T> into, ConditionChangedEvent e) {
		if (into == null)
			throw new IllegalArgumentException("Can't add a null condition !");
		int index = conditions.indexOf(from);
		if (index == -1)
			return false;
		conditions.set(index, into);
		into.setParent(this);
		from.notify(new ConditionChangedEvent(into, e.newConditionLabel())); //TODO : refactor !! shouldn't get an event as parameter
		from.setParent(null);
		return true;
	}
	
	protected void doRemove(Condition<T> what) {
		conditions.remove(what);
	}
	//@}
	
	
	/**Evaluates the given group against the given object.
	 *An empty group evaluates everything to true by default.
	 */
	public Boolean evaluate(T o) {
		if (conditions.isEmpty())
			return EMPTY_VALUE;
		if (link == Operators.AND) {
			for (Condition<T> cond : conditions)
				if (! cond.evaluate(o))
					return false;
			return true;
		} else if (link == Operators.OR) {
			for (Condition<T> cond : conditions)
				if (cond.evaluate(o))
					return true;
			return false;
		}
		throw new RuntimeException("No valid link operator found !");
	}
	
	
	/**@name	Interface compliance*/
	//@{
	public String toString() {
		return toString(PrintTypes.SPOKEN_LANGUAGE);
	}
	
	public String toString(PrintTypes type) {
		if (conditions.size() <= 0)
			return EMPTY_VALUE.toString();
		String result = "";
		int i;
		for (i = 0; i < conditions.size() - 1; i++) {
			Condition c = conditions.get(i);
			boolean addParens = ConditionGroup.class.isInstance(c);
			result +=	(addParens ? "(" : "")
						+ c.toString(type)
						+ (addParens ? ") " : " ")
						+ operator().toString(type)
						+ " ";
		}
		result += conditions.get(i).toString(type);
		return result;
	}
	
	public Iterator<Condition<T>> iterator() {
		return conditions.iterator();
	}
	
	/**@throws	RuntimeException	instead of CloneNotSupportedException*/
	@SuppressWarnings("unchecked")
	public ConditionGroup<T> clone() {
		ConditionGroup<T> result = new ConditionGroup<T>(this.operator());
		for (Condition c : conditions)
			result.add(c.clone());
		return result;
	}
	//@}
}