package search.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import search.model.Condition;
import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.view.AbstractConditionView;
import search.view.ElementaryConditionView;
import search.view.ElementaryConditionWithChoicesView;
import search.view.ConditionGroupView;
import search.view.RootConditionGroupView;
import search.event.*;
import search.client.SearchInputProvider;
import search.client.SearchListener;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class SearchController<T> implements ConditionAddedEventHandler, ViewUpdateRequestEventHandler, ConditionRemovedEventHandler, ConditionUpdatedEventHandler, ConditionChangedEventHandler {
	private ConditionGroup<T> conditions;
	private RootConditionGroupView<T> view;
	private List<SearchListener<T>> listeners = new LinkedList<SearchListener<T>>();
	private SearchInputProvider<T> input;
	boolean updateOnChange = true;
	
	@SuppressWarnings("unchecked")
	public SearchController(SearchInputProvider<T> input) {
		this(new ConditionGroup<T>().add((ElementaryCondition<T>) (input.getConditions().values().toArray()[0])), input);
	}
	
	public SearchController(ConditionGroup<T> defaultConditions, SearchInputProvider<T> input) {
		this(new RootConditionGroupView<T>(defaultConditions), defaultConditions, input);
		updateView((ConditionGroupView<T>) view, null);
	}
	
	public SearchController(RootConditionGroupView<T> view, ConditionGroup<T> defaultConditions, SearchInputProvider<T> input) {
		this.input = input;
		this.view = view;
		this.conditions = defaultConditions;
		view.addAddListener(this);
		view.addRemoveListener(this);
		view.addUpdateListener(this);
		view.addViewUpdateRequestListener(this);
		view.addConditionChangedListener(this);
	}
	
	/**@name	Getters*/
	//@{
	public RootConditionGroupView<T> getView() {
		return view;
	}
	//@}
	
	
	/**@name	Setters*/
	//@{
	public void setUpdateOnChange(boolean state) {
		updateOnChange = state;
	}
	//@}
	
	
	/**@name	Evaluation*/
	//@{
	/**
	 *@throws	NullPointerException	if no input is set
	 */
	public void evaluate() {
		evaluate(input.getData());
	}
	
	public void evaluate(List<T> items) {
		List<T> result = new LinkedList<T>();
		for (T item : items)
			if (evaluate(item))
				result.add(item);
		notifySearchListeners(result);
	}
	
	public boolean evaluate(T item) {
		return conditions.evaluate(item);
	}
	//@}
	
	
	/**@name	Listeners management*/
	//@{
	public void addSearchListener(SearchListener<T> l) {
		listeners.add(l);
	}
	
	public void removeSearchListener(SearchListener<T> l) {
		listeners.remove(l);
	}
	
	private void notifySearchListeners(List<T> result) {
		for (SearchListener<T> l : listeners)
			l.onSearchResultsRecieved(result);
	}
	//@}
	
	
	/**@name	View management*/
	//@{
	private AbstractConditionView<T> createView(Condition<T> c, String selectedLabel) {
		if (ElementaryCondition.class.isInstance(c)) {
			return new ElementaryConditionWithChoicesView<T>((ElementaryCondition<T>) c,
															 new ArrayList<String>(input.getConditions().keySet()), 
															 selectedLabel).finishSetup();
		}
		if (ConditionGroup.class.isInstance(c))
			return new ConditionGroupView<T>((ConditionGroup<T>) c);
		throw new IllegalArgumentException("I don't know this type of condition !\nClass : " + c.getClass() + " ; object : " + c);
	}
	
	private void updateView(ConditionGroupView<T> toUpdate, String selectedLabel) {
		List<AbstractConditionView<T>> views = new ArrayList<AbstractConditionView<T>>(toUpdate.model().size());
		for (Condition<T> cond : toUpdate.model()) {
			AbstractConditionView<T> view = createView(cond, selectedLabel);
			view.setParent(toUpdate);
			views.add(view);
		}
		toUpdate.updateConditionsPanel(views);
	}
	//@}
	
	/**@name	View events handling*/
	//@{
	@SuppressWarnings("unchecked")
	public void onConditionEvent(ConditionAddedEvent e) {
		System.out.println("-> Controller recieved event " + e); //DEBUG
		Condition<T> toAdd;
		if (e.what().getClass().equals(ConditionGroup.class)) {
			ConditionGroup<T> groupToAdd = (ConditionGroup<T>) e.what(); //renaming to avoid multiple casts
			if (groupToAdd.size() == 0) //empty group ?
				groupToAdd.add((Condition<T>) (e.where().clone())); //add him a child
			toAdd = groupToAdd;
		} else {
			toAdd = (ElementaryCondition<T>) (e.where().clone());
		}
		
		((ConditionGroup<T>) e.where().parent()).addAfter(toAdd, e.where());
		
		if (updateOnChange)
			evaluate(input.getData());
	}
	
	@SuppressWarnings("unchecked")
	public void onViewUpdateRequest(ViewUpdateRequestEvent e) {
		System.out.println("-> Controller recieved event " + e); //DEBUG
		ConditionGroupView<T> toUpdate = (ConditionGroupView<T>) e.getSource();

		if (e.getOrigin() == null) //TODO : refactor (immonde)
			updateView(toUpdate, null);
		else
			updateView(toUpdate, ((ConditionChangedEvent) e.getOrigin()).newConditionLabel());
		
		if (updateOnChange)
			evaluate(input.getData());
	}
	
	@SuppressWarnings("unchecked")
	public void onConditionEvent(ConditionRemovedEvent e) {
		System.out.println("-> Controller recieved event " + e); //DEBUG
		ConditionGroup<T> father = ((Condition<T>) e.what()).parent();
		ConditionGroup<T> parent = father;
		boolean allow = false;
		while (parent != null && ! allow) {
			allow = parent.size() > 1; //it is allowed to suppress this condition only if, at some point, there is a group that isn't empty
			parent = parent.parent();
		}
		if (! allow)
			return;
		parent = father;
		while (parent.size() == 1) {//let's suppress every group that is made empty because of this suppression
			parent.remove();
			parent = parent.parent();
		}
		((Condition<T>) e.what()).remove();
		
		if (updateOnChange)
			evaluate(input.getData());
	}
	
	@SuppressWarnings("unchecked")
	public void onConditionEvent(ConditionUpdatedEvent e) {
		System.out.println("-> Controller recieved event " + e); //DEBUG
		switch (e.updatedField()) {
			case OPERAND:
				((ElementaryCondition<T>) e.what()).setOperand(e.newValue());
				break;
			case ACTION:
				((ElementaryCondition<T>) e.what()).selectAction(e.newValue());
				break;
			case OPERATOR:
				((ConditionGroup<T>) e.what()).setOperator(e.newOperator());
				break;
			default:
				break;
		}
		
		if (updateOnChange)
			evaluate(input.getData());
	}

	@SuppressWarnings("unchecked")
	public void onConditionEvent(ConditionChangedEvent e) {
		System.out.println("-> Controller recieved event " + e); //DEBUG
		ElementaryCondition toReplace = ((ElementaryConditionWithChoicesView) e.getSource()).model();
		ElementaryCondition toAdd = input.getConditions().get(e.newConditionLabel()).clone(); //TODO : copy action and operand from toReplace to toAdd
		toReplace.parent().replace(toReplace, toAdd, e);
		
		if (updateOnChange)
			evaluate(input.getData());
	}
	//@}
}
