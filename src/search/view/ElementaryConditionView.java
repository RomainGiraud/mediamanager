package search.view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.*;
import java.util.List;
import java.util.LinkedList;

import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.event.*;

/**A view of an ElementaryCondition object.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class ElementaryConditionView<T> extends AbstractConditionView<T> implements ConditionUpdatedEventHandler, ConditionRemovedEventHandler {
	private JButton	addConditionButton,
					removeConditionButton,
					addGroupButton;
	private final JComboBox	actions;
	private final JTextField	operand;
	private final ElementaryCondition<T> model;
	private boolean componentsWereAdded = false;
	
	public ElementaryConditionView(ElementaryCondition<T> model) {
		super(new GridLayout(1, 0));

		this.model = model;
		model.addUpdateListener(this);
		
		//Elements init
		addConditionButton = addConditionButton();
		removeConditionButton = removeConditionButton();
		addGroupButton = addGroupButton();
		actions = new JComboBox(model.availableActions().toArray());
		operand = new JTextField();
		
		//Add events				
		final ConditionUpdatedEvent updatedEvent = new ConditionUpdatedEvent(model, ConditionAddedEvent.Field.NONE, null);
		actions.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					ElementaryConditionView.this.notify(updatedEvent.setFieldAndValue(ConditionUpdatedEvent.Field.ACTION, (String) actions.getSelectedItem()));
				}
			}
		);
		operand.addKeyListener(
			 new KeyListener() {
				 public void keyReleased(KeyEvent e) {
							   ElementaryConditionView.this.notify(updatedEvent.setFieldAndValue(ConditionUpdatedEvent.Field.OPERAND,
																								 operand.getText()));
				 }
				 public void keyPressed(KeyEvent e) {}
				 public void keyTyped(KeyEvent e) {}
			 }
		 );
	}

	/**@name	Getters*/
	//@{
	public ElementaryCondition<T> model() {
		return model;
	}
	//@}
	
	
	/**@name	UI setup*/
	//@{
	protected void addComponents() {
		//Add elements to the panel
		add(actions);
		add(operand);
		add(removeConditionButton);
		add(addConditionButton);
		add(addGroupButton);
		
		componentsWereAdded = true;
	}
	
	public ElementaryConditionView<T> finishSetup() {
		if (! componentsWereAdded)
			addComponents();
		//Register to the model
		model.addUpdateListener(this);
		model.addRemoveListener(this);
		
		update();
		showMe();
		
		return this;
	}
	//@}
	
	
	/**@name	UI updating*/
	//@{
	public void update() {
		if (! model.operand().equals(operand.getText()))
			operand.setText(model.operand().toString());
		if (! model.selectedAction().equals(actions.getSelectedItem()))
			actions.setSelectedItem(model.selectedAction());		
	}
	//@}
	
	public void onConditionEvent(ConditionUpdatedEvent e) {
		System.out.println("-> View recieved update event : " + e); //DEBUG
		update();
	}
	
	public void onConditionEvent(ConditionRemovedEvent e) {
		System.out.println("-> Elementary condition recieved event : " + e); //DEBUG
		super.onConditionEvent(e);
	}
}