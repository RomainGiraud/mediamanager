package search.view;

import java.util.List;
import java.util.LinkedList;

import javax.swing.*;
import javax.swing.JPanel;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.EventObject;

import search.model.Condition;
import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.event.*;

/**A superclass for all Condition views, that handles all events.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public abstract class AbstractConditionView<T> extends JPanel implements ConditionRemovedEventHandler {
	private List<ConditionAddedEventHandler> addListeners;
	private List<ConditionRemovedEventHandler> removeListeners;
	private List<ConditionUpdatedEventHandler> updateListeners;
	private List<ViewUpdateRequestEventHandler> viewUpdateListeners;
	private List<ConditionChangedEventHandler> conditionChangedListeners;
	private ConditionGroupView<T> parent = null;
	
	protected AbstractConditionView(LayoutManager l) {
		super(l);
		initListeners();
	}
		
	/**@name	Getters*/
	//@{
	/**Returns the model for this view.*/
	public abstract Condition<T> model();
	
	/**The parent view for this view.
	 *It is the view for the parent of this condition in the model.
	 *@returns	null	if the condition is the root of the logical tree
	 */
	public ConditionGroupView<T> parent() {
		return parent;
	}
	//@}
	
	
	/**@name	Setters*/
	//@{
	public AbstractConditionView<T> setParent(ConditionGroupView<T> parent) {
		this.parent = parent;
		return this;
	}
	//@}
	
	/**@name	Listeners management*/
	//@{
	public void initListeners() {
		addListeners = new LinkedList<ConditionAddedEventHandler>();
		removeListeners = new LinkedList<ConditionRemovedEventHandler>();
		updateListeners = new LinkedList<ConditionUpdatedEventHandler>();
		viewUpdateListeners = new LinkedList<ViewUpdateRequestEventHandler>();
		conditionChangedListeners = new LinkedList<ConditionChangedEventHandler>();
	}
	
	public void addAddListener(ConditionAddedEventHandler h) {
		addListeners.add(h);
	}
	
	public void removeAddListener(ConditionAddedEventHandler h) {
		addListeners.remove(h);
	}
	
	public void addRemoveListener(ConditionRemovedEventHandler h) {
		removeListeners.add(h);
	}
	
	public void removeRemoveListener(ConditionRemovedEventHandler h) {
		removeListeners.remove(h);
	}
	
	public void addUpdateListener(ConditionUpdatedEventHandler h) {
		updateListeners.add(h);
	}
	
	public void removeUpdateListener(ConditionUpdatedEventHandler h) {
		updateListeners.remove(h);
	}
	
	public void addViewUpdateRequestListener(ViewUpdateRequestEventHandler h) {
		viewUpdateListeners.add(h);
	}
	
	public void removeViewUpdateRequestListener(ViewUpdateRequestEventHandler h) {
		viewUpdateListeners.remove(h);
	}
	
	public void addConditionChangedListener(ConditionChangedEventHandler h) {
		conditionChangedListeners.add(h);
	}
	
	public void removeConditionChangedListener(ConditionChangedEventHandler h) {
		conditionChangedListeners.remove(h);
	}
	
	public void notify(ConditionAddedEvent e) { //TODO : put back to protected once ConditionChanged will handle everything
		System.out.println("View emitted event " + e);
		for (ConditionAddedEventHandler h : addListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	
	public void notify(ConditionRemovedEvent e) { //TODO : put back to protected once ConditionChanged will handle everything
		System.out.println("View emitted event " + e);
		for (ConditionRemovedEventHandler h : removeListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	
	protected void notify(ConditionUpdatedEvent e) {
		System.out.println("View emitted event " + e);
		for (ConditionUpdatedEventHandler h : updateListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	
	protected void notify(ViewUpdateRequestEvent e) {
		System.out.println("View emitted event " + e);
		for (ViewUpdateRequestEventHandler h : viewUpdateListeners)
			h.onViewUpdateRequest(e);
		if (parent != null)
			parent.notify(e);
	}
	
	protected void notify(ConditionChangedEvent e) {
		System.out.println("View emitted event " + e);
		for (ConditionChangedEventHandler h : conditionChangedListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	//@}
	
	/**@name	Components factory
	 *Allows us to have a more consistent UI.
	 */
	//@{
	/**Returns a full-fledged button that sends an event asking for an ElementaryCondition to be added upon clicking.
	 */
	public JButton addConditionButton() {
		JButton result = new JButton("+");
		result.addActionListener(
			new ActionListener() {
				 public void actionPerformed(ActionEvent ae) {
					AbstractConditionView.this.notify(new ConditionAddedEvent(AbstractConditionView.this.model(), AbstractConditionView.this.model()));
				}
			}
		);
		return result;
	}
	
	/**Returns a full-fledged button that sends an event asking for a GroupCondition to be added upon clicking.
	 */
	public JButton addGroupButton() {
		JButton result = new JButton("++");
		result.addActionListener(
			 new ActionListener() {
				 public void actionPerformed(ActionEvent ae) {
					 AbstractConditionView.this.notify(new ConditionAddedEvent(new ConditionGroup<T>(), AbstractConditionView.this.model()));
				 }
			 }
		 );
		return result;
	}
	
	/**Returns a full-fledged button that sends an event asking for this condition to be removed upon clicking.
	 */
	public JButton removeConditionButton() {
		JButton result = new JButton("-");
		result.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					AbstractConditionView.this.notify(new ConditionRemovedEvent(AbstractConditionView.this.model()));
				}
			}
		);
		return result;
	}
	
	public static JToggleButton toggleButton() {
		return new JToggleButton(">", true);
	}
	//@}
	
	public void onConditionEvent(ConditionRemovedEvent e) {
		if (e.what() == model())
			destroy();
	}		
	
	protected void destroy() {
		setParent(null);
		initListeners();
	}
	
	/**@name	UI updating*/
	//@{
	protected void hideMe() {
		this.setVisible(false);
	}
	
	protected void showMe() {
		this.setVisible(true);
	}
	//@}
}