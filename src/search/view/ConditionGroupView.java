package search.view;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

import search.model.Condition;
import search.model.ConditionGroup;
import search.model.Operators;
import search.model.PrintTypes;
import search.event.*;

/**A view of a group of conditions.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class ConditionGroupView<T> extends AbstractConditionView<T> implements ConditionRemovedEventHandler, ConditionUpdatedEventHandler, ConditionAddedEventHandler, ConditionChangedEventHandler {
	
	protected final JPanel topPanel = new JPanel(new GridLayout(1, 0));
	protected final JToggleButton toggle;
	protected PrintTypes printType = PrintTypes.SPOKEN_LANGUAGE;
	protected JPanel conditionsPanel;
	protected JButton	delete;
	protected JTextField	description;
	protected List<JToggleButton> operatorButtons;
	protected List<JLabel> operatorLabels = new LinkedList<JLabel>();
	protected List<AbstractConditionView<T>> conditionsViews = new LinkedList<AbstractConditionView<T>>();
	
	private final ConditionGroup<T> model;

	
	public ConditionGroupView(ConditionGroup<T> model) {
		super(new BorderLayout());
		
		this.model = model;

		//Top panel (group controls)
		//	elements init
		toggle = toggleButton();
		delete = removeConditionButton();
		
		description = new JTextField();
		description.setEnabled(false);
		updateDescription();
			
		//	elements adding
		topPanel.add(toggle);
		for (JToggleButton operatorButton : operatorButtons())
			topPanel.add(operatorButton);
		
		updateOperatorButtonsLabels();
		updateOperatorButtons();
		
		topPanel.add(description);
		topPanel.add(delete);
		
		//	merging with main panel
		add(topPanel, BorderLayout.NORTH);
		
		//Elementary conditions panel
		conditionsPanel = new JPanel(new GridLayout(0, 1));
		add(conditionsPanel, BorderLayout.CENTER);
		
		//Display events handling
		toggle.addChangeListener(new ChangeListener() {
									public void stateChanged(ChangeEvent e) {
										conditionsPanel.setVisible(! conditionsPanel.isVisible());
									 }
								 });
		
		//Register to the model
		model.addUpdateListener(this);
		model.addRemoveListener(this);
		model.addAddListener(this);
		model.addConditionChangedListener(this);
				
		showMe();
	}
	
	/**@name	Getters*/
	//@{
	public ConditionGroup<T> model() {
		return model;
	}

	private List<JToggleButton> operatorButtons() {
		if (operatorButtons != null)
			return operatorButtons;
		
		Operators[] ops = Operators.values();
		List<JToggleButton> result = new ArrayList<JToggleButton>(ops.length);
		for (final Operators op : ops) {
			JToggleButton opButton = new JToggleButton();
			opButton.setActionCommand(op.toString(PrintTypes.BOOLEAN));
			opButton.addActionListener(
			   new ActionListener() {
				   public void actionPerformed(ActionEvent ae) {
					   ConditionGroupView.this.selectOperator(op);
					   ConditionGroupView.this.notify(new ConditionUpdatedEvent(ConditionGroupView.this.model, ConditionUpdatedEvent.Field.OPERATOR, op));
				   }
			   }
		   );
			result.add(opButton);
		}
		
		operatorButtons = result;
		return result;
	}
	
	public PrintTypes printType() {
		return printType;
	}
	//@}
	
	
	/**@name	UI updating*/
	//@{
	public void setPrintType(PrintTypes type) {
		if (type == printType())
			return;
		for (AbstractConditionView view : conditionsViews)
			if (ConditionGroupView.class.isInstance(view))
				((ConditionGroupView) view).setPrintType(type);
		updateDescription(type);
		updateOperatorButtonsLabels(type);
		updateOperatorLabels(type);
		printType = type;
	}
	
	private void updateDescription() {
		updateDescription(printType());
	}
	
	private void updateDescription(PrintTypes type) {
		description.setText(model.toString(type));
	}
	
	private void updateOperatorButtonsLabels() {
		updateOperatorButtonsLabels(printType());
	}
	
	private void updateOperatorButtonsLabels(PrintTypes type) {
		int i = 0;
		for (Operators op : Operators.values()) {
			operatorButtons().get(i).setText(op.toString(type));
			i++;
		}
	}
	
	private void updateOperatorButtons() {
		selectOperator(model.operator());
	}
	
	private void updateOperatorLabels() {	
		updateOperatorLabels(printType());
	}
	
	private void updateOperatorLabels(PrintTypes type) {
		String operator = model().operator().toString(type);
		for (JLabel operatorLabel : operatorLabels)
			operatorLabel.setText(operator);
	}
	
	private void selectOperator(Operators op) {
		for (JToggleButton b : operatorButtons())
			if (! b.getActionCommand().equals(op.toString(PrintTypes.BOOLEAN)))
				b.setSelected(false);
			else
				b.setSelected(true);
		updateOperatorLabels();
	}
	
	public void updateConditionsPanel(List<AbstractConditionView<T>> views) {
		conditionsPanel.removeAll();
		for (AbstractConditionView view : conditionsViews)
			view.destroy();
		conditionsViews = views;
		operatorLabels.clear();
		boolean addLabel = false; //the first condition doesn't have any operator label
		for (AbstractConditionView<T> view : views) {
			JPanel conditionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			JLabel opLabel;
			if (addLabel) {
				opLabel = new JLabel(model().operator().toString(printType()));
				operatorLabels.add(opLabel);
			} else {
				String whitespace = ""; //ok, so, since all but the first condition have the name of the operator in front of themselves, we add some whitespace in front of the first condition, to have them all vertically aligned
				for (int i = 0; i < model().operator().toString(printType()).length(); i++) //since the size of an operator is variable, we put a space for every letter of the current operator/printType couple
					whitespace += " ";
				opLabel = new JLabel(whitespace); //that's it !
				addLabel = true;
			}
			conditionPanel.add(opLabel);
			conditionPanel.add(view);
			conditionsPanel.add(conditionPanel);
		}
		revalidate();
	}
	
	public AbstractConditionView<T> setParent(ConditionGroupView<T> newParent) {
		AbstractConditionView<T> result = super.setParent(newParent);
		notify(new ViewUpdateRequestEvent(this));
		return result;
	}
	//@}
	
	
	/**@name	Events handling*/
	//@{
	public void onConditionEvent(ConditionUpdatedEvent e) {
		System.out.println("-> Group recieved event : " + e); //DEBUG
		if (e.what() == this.model() && e.updatedField() == ConditionUpdatedEvent.Field.OPERATOR)
			selectOperator(e.newOperator());
		updateDescription();
	}
	
	public void onConditionEvent(ConditionRemovedEvent e) {
		System.out.println("-> Group recieved event : " + e); //DEBUG
		updateDescription();
		notify(new ViewUpdateRequestEvent(this));
	}
	
	@SuppressWarnings("unchecked")
	public void onConditionEvent(ConditionAddedEvent e) {
		System.out.println("-> Group recieved event : " + e); //DEBUG
		if (model().contains(e.where()))
			notify(new ViewUpdateRequestEvent(this));
		updateDescription();
	}
	
 	public void onConditionEvent(ConditionChangedEvent e) {
		updateDescription();
	}

	//@}
}