package search.view;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

import search.model.Condition;
import search.model.ConditionGroup;
import search.model.Operators;
import search.model.PrintTypes;
import search.event.*;

/**A view for the root of a tree of conditions.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class RootConditionGroupView<T> extends ConditionGroupView<T> {
	private JComboBox descriptionType = null;
	
	public RootConditionGroupView(ConditionGroup<T> model) {
		super(model);
		
		topPanel.remove(delete);
		
		if (descriptionType == null) {
			descriptionType = new JComboBox();
			for (PrintTypes type : PrintTypes.values())
				descriptionType.addItem(type);
		}
		descriptionType.addItemListener(new ItemListener() {
											public void itemStateChanged(ItemEvent e) {
												RootConditionGroupView.this.setPrintType(((JComboBox) e.getSource()).getSelectedIndex());
											}
										  });
		
		topPanel.add(descriptionType);
	}
	
	public void setPrintType(int i) {
		setPrintType(PrintTypes.values()[i]);
	}
	//@}
}