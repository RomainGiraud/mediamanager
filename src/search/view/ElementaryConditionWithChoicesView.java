package search.view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.util.List;
import java.util.LinkedList;

import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.event.*;

/**A view of an ElementaryCondition object, with a choice to change it for another condition.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

public class ElementaryConditionWithChoicesView<T> extends ElementaryConditionView<T> implements ConditionChangedEventHandler {
	private final JComboBox	actions;
	private List<String> conditionsLabels;
	
	public ElementaryConditionWithChoicesView(ElementaryCondition<T> model, List<String> conditionsLabels) {
		this(model, conditionsLabels, conditionsLabels.get(0));
	}
	
	/**
	 *@throws	IllegalArgumentException or ArrayIndexOutOfBounds	if the given list of actions is empty
	 */
	public ElementaryConditionWithChoicesView(ElementaryCondition<T> model, List<String> conditionsLabels, String selectedCondition) {
		super(model);
		
		if (conditionsLabels.isEmpty())
			throw new IllegalArgumentException("List of actions can't be empty !");
		if (! conditionsLabels.contains(selectedCondition))
			selectedCondition = conditionsLabels.get(0);
			
		actions = new JComboBox(conditionsLabels.toArray());
		this.conditionsLabels = conditionsLabels;

		actions.setSelectedItem(selectedCondition);

		actions.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent e) {
					ElementaryConditionWithChoicesView.this.notify(new ConditionChangedEvent(ElementaryConditionWithChoicesView.this, (String) actions.getSelectedItem()));
			   }
		   }
	    );
		model.addConditionChangedListener(this);
	}
	
	public String selectedCondition() {
		return (String) actions.getSelectedItem();
	}
	
	protected void addComponents() {
		add(actions);
		super.addComponents();
	}
	
	public void onConditionEvent(ConditionChangedEvent e) {
		System.out.println("-> ECWithChoicesView recieved event " + e);
		if (this.parent() != null) //TODO : WTF ??
			notify(new ViewUpdateRequestEvent(this.parent(), e));
	}
}