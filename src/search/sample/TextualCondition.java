package search.sample;

import search.model.ElementaryCondition;

import java.util.List;
import java.util.ArrayList;

/**An ElementaryCondition that evaluates textual conditions.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public abstract class TextualCondition<T> extends ElementaryCondition<T> {
	protected boolean caseSensitive = false;
	
	protected enum TextAction {
		CONTAINS("contient"),
		CONTAINS_NOT("ne contient pas"),
		IS("est"),
		IS_NOT("n'est pas");
		
		private String name;
		
		TextAction(String name) {
			this.name = name;
		}
		
		String label() {
			return name;
		}
		
		protected static List<String> labels() {
			List<String> result = new ArrayList<String>(values().length);
			for (TextAction a : values())
				result.add(a.label());
			return result;
		}
		
	}
	
	protected TextualCondition() {
		super(TextAction.labels());
	}
	
	protected TextualCondition(List<String> notUsed) {
		super(TextAction.labels());
	}
	
	protected Boolean eval(String s) {
		return eval(s, selectedAction());
	}
	
	protected Boolean eval(String s, String act) {
		if (act.equals(TextAction.CONTAINS.label()))
			return (caseSensitive ? s : s.toLowerCase()).contains(caseSensitive ? operand().toString() : operand().toString().toLowerCase());
		if (act.equals(TextAction.CONTAINS_NOT.label()))
			return ! (caseSensitive ? s : s.toLowerCase()).contains(caseSensitive ? operand().toString() : operand().toString().toLowerCase());
		if (act.equals(TextAction.IS.label()))
			return (caseSensitive ? s : s.toLowerCase()).equals(caseSensitive ? operand().toString() : operand().toString().toLowerCase());
		if (act.equals(TextAction.IS_NOT.label()))
			return ! (caseSensitive ? s : s.toLowerCase()).equals(caseSensitive ? operand().toString() : operand().toString().toLowerCase());
		throw new IllegalArgumentException("The action \"" + selectedAction() + "\" isn't an allowed value !");
	}
}