package search.event;

import search.model.Condition;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class ConditionAddedEvent extends ConditionUpdatedEvent {
	private Condition where;
	
	public ConditionAddedEvent(Condition what, Condition where) {
		super(what);
		this.where = where;
	}
	
	/**Returns the condition above the newly added one.
	 *May return the same value as what() if the added condition is the first of the group (there's no previous one, so it is itself the previous one).
	 *@see	what	to get the newly added condition
	 */
	public Condition where() {
		return where;
	}
}