package search.event;

import search.model.Condition;
import search.model.ConditionGroup;
import search.model.ElementaryCondition;
import search.model.Operators;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class ConditionUpdatedEvent {
	private Condition what;
	private Field which;
	private String newValue;
	private Operators newOperator;
	
	protected ConditionUpdatedEvent(Condition what) {
		this(what, Field.NONE, null); //inheriting classes compatibility
	}
	
	/**
	 *@throws	IllegalArgumentException	if the "value" parameter's real type isn't one correct for the Field modified, or if the given field is illegal for the condition type (i.e. trying to modify an ElementaryCondition's Operator).
	 */
	public ConditionUpdatedEvent(Condition what, Field f, Object value) {
		this.what = what;
		setFieldAndValue(f, value);
	}	
	
	/**@name	Getters*/
	//@{
	public Condition what() {
		return what;
	}
	
	public Field updatedField() {
		return which;
	}
	
	public String newValue() {
		return newValue;
	}
	
	public Operators newOperator() {
		return newOperator;
	}
	//@}
	
	
	/**@name	Setters*/
	//@{
	public ConditionUpdatedEvent setFieldAndValue(ConditionUpdatedEvent.Field f, Object value) {
		if (f != Field.NONE) { //inheriting classes compatibility
			if (value.getClass() != f.type())
				throw new IllegalArgumentException("Given value isn't allowed for the given modified field !");
			if (f == Field.OPERAND || f == Field.ACTION) {
				if (! ElementaryCondition.class.isInstance(what))
					throw new IllegalArgumentException("Given field (" + f + ") isn't allowed for the given condition type (" + what.getClass() + ") !");
				newValue = (String) value;
			}
			else if (f == Field.OPERATOR) {
				if (! ConditionGroup.class.isInstance(what))
					throw new IllegalArgumentException("Given field (" + f + ") isn't allowed for the given condition type (" + what.getClass() + ") !");
				newOperator = (Operators) value;
			}
		}
		which = f;
		return this;
	}
	//@}
	
	/**Represents one of the fields of a Condition, and its associated type.
	*/
	public enum Field {
		NONE(null), //inheriting classes compatibility
		OPERAND(String.class),
		ACTION(String.class),
		OPERATOR(Operators.class);
		
		private Class valueType;
		
		private Field(Class type) {
			valueType = type;
		}
		
		public Class type() {
			return valueType;
		}
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "(" + what + ", " + which + ", [value=" + newValue + " | operator=" + newOperator + "])";
	}
}