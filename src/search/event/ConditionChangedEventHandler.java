package search.event;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public interface ConditionChangedEventHandler {
	public void onConditionEvent(ConditionChangedEvent e);
}