package search.event;

import java.util.List;
import java.util.LinkedList;

import search.model.ConditionGroup;

/**An abstract class that provides everything needed to send ConditionEvents and manage listeners.
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public abstract class ConditionEventsSource<T> {
	/**Parent in the conditions hierarchy tree.
	 *If null, then this condition is the hierarchy root.
	 */
	private ConditionGroup<T> parent;
	
	private List<ConditionAddedEventHandler> addListeners;
	private List<ConditionRemovedEventHandler> removeListeners;
	private List<ConditionUpdatedEventHandler> updateListeners;
	private List<ConditionChangedEventHandler> conditionChangedListeners;
	
	protected ConditionEventsSource() {
		initListeners();
	}

	public ConditionGroup<T> parent() {
		return parent;
	}
	
	public void setParent(ConditionGroup<T> parent) {
		this.parent = parent;
	}
	
	
	/**@name	Listeners management*/
	//@{
	protected void initListeners() {
		addListeners = new LinkedList<ConditionAddedEventHandler>();
		removeListeners = new LinkedList<ConditionRemovedEventHandler>();
		updateListeners = new LinkedList<ConditionUpdatedEventHandler>();
		conditionChangedListeners = new LinkedList<ConditionChangedEventHandler>();
	}
	
	public void addAddListener(ConditionAddedEventHandler h) {
		addListeners.add(h);
	}
	
	public void removeAddListener(ConditionAddedEventHandler h) {
		addListeners.remove(h);
	}
	
	public void addRemoveListener(ConditionRemovedEventHandler h) {
		removeListeners.add(h);
	}
	
	public void removeRemoveListener(ConditionRemovedEventHandler h) {
		removeListeners.remove(h);
	}
	
	public void addUpdateListener(ConditionUpdatedEventHandler h) {
		updateListeners.add(h);
	}
	
	public void removeUpdateListener(ConditionUpdatedEventHandler h) {
		updateListeners.remove(h);
	}

	public void addConditionChangedListener(ConditionChangedEventHandler h) {
		conditionChangedListeners.add(h);
	}
	
	public void removeConditionChangedListener(ConditionChangedEventHandler h) {
		conditionChangedListeners.remove(h);
	}
	
	protected void notify(ConditionAddedEvent e) {
		System.out.println("Model sent event " + e); //DEBUG
		for (ConditionAddedEventHandler h : addListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	
	protected void notify(ConditionRemovedEvent e) {
		System.out.println("Model sent event " + e); //DEBUG
		for (ConditionRemovedEventHandler h : removeListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	
	protected void notify(ConditionUpdatedEvent e) {
		System.out.println("Model sent event " + e); //DEBUG
		for (ConditionUpdatedEventHandler h : updateListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	
	public void notify(ConditionChangedEvent e) { //TODO : put back protected access once the model will correctly notify upon replacement
		System.out.println("Model sent event " + e); //DEBUG
		for (ConditionChangedEventHandler h : conditionChangedListeners)
			h.onConditionEvent(e);
		if (parent != null)
			parent.notify(e);
	}
	//@}
}