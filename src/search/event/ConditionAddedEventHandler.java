package search.event;

import search.model.Condition;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public interface ConditionAddedEventHandler {
	public void onConditionEvent(ConditionAddedEvent c);
}