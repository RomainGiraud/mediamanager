package search.event;

import java.util.EventObject;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class ViewUpdateRequestEvent extends EventObject {
	Object origin;
	
	public ViewUpdateRequestEvent(Object source) {
		this(source, null);
	}
	
	/**Allows us to have another argument.
	 *The "origin" is the reason why an update was requested.
	 */
	public ViewUpdateRequestEvent(Object source, Object origin) {
		super(source);
		this.origin = origin;
	}
	
	public Object getOrigin() {
		return origin;
	}
}