package search.event;

import java.util.EventObject;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public class ConditionChangedEvent extends EventObject {
	String newConditionLabel;
	
	public ConditionChangedEvent(Object source, String newConditionLabel) {
		super(source);
		this.newConditionLabel = newConditionLabel;
	}
	
	public String newConditionLabel() {
		return newConditionLabel;
	}
	
	public String toString() {
		return super.toString() + " ; newConditionLabel = " + newConditionLabel;
	}
}