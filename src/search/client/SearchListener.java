package search.client;

import java.util.List;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public interface SearchListener<T> {
	public void onSearchResultsRecieved(List<T> results);
}