package search.client;

import java.util.List;
import java.util.Map;

import search.model.ElementaryCondition;

/**
*@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
*@version 0.1
*/

public interface SearchInputProvider<T> {
	public Map<String, ElementaryCondition<T>> getConditions();
	public List<T> getData();
}