# Éxécution

Pour exécuter ce programme :
- ajouter les bibliothèques suivantes au dossier 'lib' :
	- CORESE_2009_16_04_v2_4_1_14.jar
	- jdom.jar
	- substance.jar (téléchargeable sur http://substance.dev.java.net)
- compiler avec 'ant compile'
- exécuter le fichier 'media_manager'

Pour exécuter les tests unitaires, ajouter JUnit 4.5 aux bibliothèques, et exécuter 'ant junit'.

Projet réalisé par :

# Auteurs

- [Romain Giraud](http://romaingiraud.com)
- [Matti Schneider-Ghibaudo](http://mattischneider.fr)
