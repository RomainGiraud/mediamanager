package test.search.model;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import search.model.Condition;
import search.model.ElementaryCondition;
import search.model.ConditionGroup;
import search.model.Operators;
import search.model.PrintTypes;

public class ConditionGroupTest extends ElementaryConditionTest {
	
	private ConditionGroup<String> subject;
	
	@Before
	public void setUp2() {
		subject = new ConditionGroup<String>(Operators.AND);
		addConditions();
	}
	
	public void addConditions() {
		subject.add(isNotEmpty);
		subject.add(contains);
		subject.add(equalsOperand.setOperand(TEST_STRING + CONTAINED_STRING));
	}
	
	@Test
	public void addAfterTest() {
		int i = 0;
		Condition[] conds = {isNotEmpty, contains, equalsOperand};
		for (Condition c : subject)
			assertSame("Pre-check doesn't give the correct conditions order. Please check the test itself.", conds[i++], c);
		Condition<String> newCond = new ElementaryCondition<String>() {
			public Boolean eval(String s) {
				return true;
			}
			public String toString() {
				return "***INSERTED CONDITION***";
			}
			protected String prefix(PrintTypes type) {
				return "Always true";
			}
		};
		
		subject.addAfter(newCond, equalsOperand); //insert at the end
		Condition prevCond = null;
		for (Condition c : subject)
			prevCond = c;
		assertSame("Adding at the end doesn't work : " + subject, newCond, prevCond);
		
		setUp2();
		subject.addAfter(newCond, contains); //insert in the middle
		for (Condition c : subject) {
			if (c == newCond)
				assertSame("Condition wasn't added at its asked place : " + subject, prevCond, contains);
			prevCond = c;
		}
		
		setUp2();
		subject.addAfter(newCond, null); //should insert at top
		i = 0;
		for (Condition c : subject) {
			if (i == 0)
				assertSame("First condition isn't the inserted one : " + subject, newCond, c);
			else
				assertNotSame("Inserted condition was inserted elsewhere than at first place : " + subject, newCond, c);
			i++;
		}
	}
	
	@Test
	public void constructorTest() {
		assertNotNull("Subject is null !", subject);
	}
	
	@Test
	public void andCombinationTest() {
		assertTrue("One of the tests failed.", subject.evaluate(TEST_STRING + CONTAINED_STRING));
		assertFalse("Containing test should have failed.", subject.evaluate(TEST_STRING));
		assertFalse("Equality test should have failed.", subject.evaluate(CONTAINED_STRING));
	}
	
	@Test
	public void orCombinationTest() {
		subject = new ConditionGroup<String>(Operators.OR);
		addConditions();
		assertTrue("All of the tests failed.", subject.evaluate(TEST_STRING + CONTAINED_STRING));
		assertTrue("NotEmpty test should have succeeded.", subject.evaluate(TEST_STRING));
		assertFalse("All tests should have failed.", subject.evaluate(""));
	}
	
	@Test
	public void operatorChangeTest() {
		assertFalse("Containing test should have failed.", subject.evaluate(TEST_STRING));
		subject.setOperator(Operators.OR);
		assertTrue("NotEmpty test should have reached.", subject.evaluate(TEST_STRING));
	}
	
	@Test
	public void cloneTest() {
		try {
			subject = subject.clone();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Cloning raised an exception : " + e);
		}
		constructorTest();
		andCombinationTest();
		operatorChangeTest();
	}
	
	@Test
	public void groupingGroupsTest() {
		ConditionGroup<String> group = new ConditionGroup<String>(Operators.OR);
		group.add(subject);
		assertFalse("A group containing another group doesn't return the same value as its only son !", group.evaluate(TEST_STRING));
		group.add(isNotEmpty);
		assertTrue("A group containing another group doesn't return the same value as its second son !", group.evaluate(TEST_STRING));
	}
	
	@Test
	public void emptyToStringTest() {
		subject = new ConditionGroup<String>();
		assertEquals(subject.EMPTY_VALUE.toString(), subject.toString(PrintTypes.SPOKEN_LANGUAGE));
	}
}