package test.search.model;

/**A set of tests for the Condition class.
 *@author <a href="mailto:alcmene.gs@gmail.com">Matti Schneider-Ghibaudo</a>
 *@version 0.1
 */

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

import java.lang.reflect.Method;

import search.model.ElementaryCondition;
import search.model.PrintTypes;

public class ElementaryConditionTest {
	protected final static String	TEST_STRING = "Zlagada",
									CONTAINED_STRING = "b",
									SUBSTRING_ACTION_LABEL = "substring",
									TRIM_ACTION_LABEL = "trim";
	
	protected ElementaryCondition<String>	isNotEmpty,
											contains,
											equalsOperand,
											equalsTestStringWithPreviousAction;
	
	@Before
	public void setUp() {
		isNotEmpty = new ElementaryCondition<String>() {
			public Boolean eval(String s) {
				return s.length() > 0;
			}
			protected String prefix(PrintTypes type) {
				return "The string";
			}
		};
		contains = new ElementaryCondition<String>() {
			public Boolean eval(String s) {
				return s.contains(CONTAINED_STRING);
			}
			protected String prefix(PrintTypes type) {
				return "The string";
			}
		};
		equalsOperand = new ElementaryCondition<String>() {
			public Boolean eval(String s) {
				return s.equals(this.operand());
			}
			protected String prefix(PrintTypes type) {
				return "The string";
			}
		};
		equalsTestStringWithPreviousAction = new ElementaryCondition<String>() {
			public Boolean eval(String s) {
				if (selectedAction() == SUBSTRING_ACTION_LABEL)
					s = s.substring(TEST_STRING.length() - 1);
				else if (selectedAction() == TRIM_ACTION_LABEL)
					s = s.trim();
				return s.equals(TEST_STRING);
			}
			protected String prefix(PrintTypes type) {
				return "The string";
			}
		};
	}
	
	@Test
	public void constructorTest() {
		assertNotNull("Producted a null object !", isNotEmpty);
		assertNotNull("Producted a null object !", contains);
		assertNotNull("Producted a null object !", equalsOperand);
	}
	
	@Test
	public void evaluationTest() {
		assertTrue("\"" + TEST_STRING + "\" considered empty !", isNotEmpty.evaluate(TEST_STRING));
		assertFalse("Empty string considered not empty !", isNotEmpty.evaluate(""));
		assertTrue("\"" + TEST_STRING + CONTAINED_STRING + "\" considered as not containing \"a\" !", contains.evaluate(TEST_STRING + CONTAINED_STRING));
		assertFalse("\"" + TEST_STRING + "\" said to contain \"" + CONTAINED_STRING + "\" !", contains.evaluate(TEST_STRING));
		assertFalse("Empty string said to contain \"" + CONTAINED_STRING + "\" !", contains.evaluate(""));
		equalsOperand.setOperand(TEST_STRING);
		assertTrue("The same String isn't said to equal itself !", equalsOperand.evaluate(TEST_STRING));
		assertFalse("String with suffix said to equal itself without suffix !", equalsOperand.evaluate(TEST_STRING + CONTAINED_STRING));
	}
	
	@Test
	public void updateOperandTest() {
		equalsOperand.setOperand(TEST_STRING);
		assertTrue("Method is already defective ; please fix evaluationTest before proceeding to fixing operand updating.", equalsOperand.evaluate(TEST_STRING));
		equalsOperand.setOperand(TEST_STRING + CONTAINED_STRING);
		assertFalse("Operand updating doesn't work correctly.", equalsOperand.evaluate(TEST_STRING));
	}
	
	@Test
	public void cloneTest() {
		try {
			isNotEmpty = isNotEmpty.clone();
			contains = contains.clone();
			equalsOperand = equalsOperand.clone();
		} catch (Exception e) {
			fail("Cloning raised an exception.");
		}		
		constructorTest();
		evaluationTest();
	}
	
	@Test
	public void actionTest() {
		assertTrue("Method is already defective ; please fix evaluationTest before proceeding to action selection fixing.", equalsTestStringWithPreviousAction.evaluate(TEST_STRING));
		List<String> actions = new ArrayList<String>(2);
		actions.add(SUBSTRING_ACTION_LABEL);
		actions.add(TRIM_ACTION_LABEL);
		equalsTestStringWithPreviousAction.setActions(actions);
		assertTrue("Adding actions without selecting one changes behavior.", equalsTestStringWithPreviousAction.evaluate(TEST_STRING));
		equalsTestStringWithPreviousAction.selectAction(SUBSTRING_ACTION_LABEL);
		assertFalse("Substring'd string isn't considered as empty !", equalsTestStringWithPreviousAction.evaluate(TEST_STRING));
		equalsTestStringWithPreviousAction.selectAction(TRIM_ACTION_LABEL);
		assertTrue("Selecting an innocent action changes behavior !", equalsTestStringWithPreviousAction.evaluate(TEST_STRING));
	}
}