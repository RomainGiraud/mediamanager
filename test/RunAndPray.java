package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	test.search.model.ElementaryConditionTest.class,
	test.search.model.ConditionGroupTest.class
})

public class RunAndPray {}
